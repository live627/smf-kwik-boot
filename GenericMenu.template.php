<?php
/**
 * Simple Machines Forum (SMF)
 *
 * @package SMF
 * @author Simple Machines http://www.simplemachines.org
 * @copyright 2014 Simple Machines and individual contributors
 * @license http://www.simplemachines.org/about/smf/license.php BSD
 *
 * @version 2.1 Alpha 1
 */

// This contains the html for the side bar of the admin center, which is used for all admin pages.
function template_generic_menu_dropdown_above()
{
	global $context, $scripturl, $txt, $modSettings;

	// If we're on the admin index, quit right away.
	if (isset($context['admin_area']) && $context['admin_area'] == 'index')
		return;

	// Which menu are we rendering?
	$context['cur_menu_id'] = isset($context['cur_menu_id']) ? $context['cur_menu_id'] + 1 : 1;
	$menu_context = &$context['menu_data_' . $context['cur_menu_id']];

	echo '
			<div class="navbar navbar-default visible-xs visible-sm" role="navigation">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".adm-navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<span class="visible-xs navbar-brand">Sidebar menu</span>
				</div>
				<div class="navbar-collapse collapse adm-navbar-collapse">
					<ul class="nav navbar-nav">';

	// Main areas first.
	foreach ($menu_context['sections'] as $section)
	{
		echo '
						<li class="';

		if ($section['id'] == $menu_context['current_section'])
			echo 'active';

		echo 'dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">', $section['title'], ' <b class="caret"></b></a>
							<ul class="dropdown-menu" role="menu">';

		// For every area of this section show a link to that area (bold if it's currently selected.)
		$additional_items = 0;
		foreach ($section['areas'] as $i => $area)
		{
			// Not supposed to be printed?
			if (empty($area['label']))
				continue;

			echo '
					<li class="';

			// Is this the current area, or just some area?
			if ($i == $menu_context['current_area'])
			{
				echo 'active';

				if (empty($context['tabs']))
					$context['tabs'] = isset($area['subsections']) ? $area['subsections'] : array();
			}

			if (isset($area['subsections']))
				echo ' dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">';
			else
				echo '">
						<a href="', isset($area['url']) ? $area['url'] : $menu_context['base_url'] . ';area=' . $i, $menu_context['extra_parameters'], '">';

			echo '<i class="fa fa-', $area['icon_class'],' fa-fw"></i>', $area['label'];

			if (isset($area['subsections']))
				echo ' <b class="caret"></b>';

			echo '</a>';

			// Is there any subsections?
			if (!empty($area['subsections']))
			{
				echo '
						<ul class="dropdown-menu" role="menu">';

				foreach ($area['subsections'] as $sa => $sub)
				{
					if (!empty($sub['disabled']))
						continue;

					$url = isset($sub['url']) ? $sub['url'] : (isset($area['url']) ? $area['url'] : $menu_context['base_url'] . ';area=' . $i) . ';sa=' . $sa;

					echo '
								<li';

					if (!empty($sub['selected']))
						echo ' class="active"';

					echo '><a href="', $url, $menu_context['extra_parameters'], '">', $sub['label'], '</a>
							</li>';
				}

				echo '
						</ul>';
			}

			echo '
					</li>';
		}
		echo '
				</ul>
			</li>';
	}

	echo '
	</ul>

				</div><!--/.nav-collapse -->
			</div>
	<div class="row">
		<div class="col-md-3 visible-md visible-lg">
			<div class="sidebar-nav">
				<div class="dropdown open">
					<ul class="dropdown-menu">';

	// Main areas first.
	foreach ($menu_context['sections'] as $section)
	{
		echo '
						<li class="nav-header">', $section['title'], '</li>
						<li class="divider"></li>';

		// For every area of this section show a link to that area (bold if it's currently selected.)
		$additional_items = 0;
		foreach ($section['areas'] as $i => $area)
		{
			// Not supposed to be printed?
			if (empty($area['label']))
				continue;

			echo '
					<li class="';

			// Is this the current area, or just some area?
			if ($i == $menu_context['current_area'])
				echo 'active';

			if (isset($area['subsections']))
				echo ' dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">';
			else
				echo '">
						<a href="', isset($area['url']) ? $area['url'] : $menu_context['base_url'] . ';area=' . $i, $menu_context['extra_parameters'], '">';

			echo $area['icon'], $area['label'];

			if (isset($area['subsections']))
				echo ' <b class="caret"></b>';

			echo '</a>';

			// Is there any subsections?
			if (!empty($area['subsections']))
			{
				echo '
						<ul class="dropdown-menu" role="menu">';

				foreach ($area['subsections'] as $sa => $sub)
				{
					if (!empty($sub['disabled']))
						continue;

					$url = isset($sub['url']) ? $sub['url'] : (isset($area['url']) ? $area['url'] : $menu_context['base_url'] . ';area=' . $i) . ';sa=' . $sa;

					echo '
								<li';

					if (!empty($sub['selected']))
						echo ' class="active"';

					echo '><a href="', $url, $menu_context['extra_parameters'], '">', $sub['label'], '</a>
								</li>';
				}

				echo '
							</ul>';
			}
		}
		echo '
						</li>';
	}

			echo '
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-9">';


	// It's possible that some pages have their own tabs they wanna force...
// 	if (!empty($context['tabs']))
		template_generic_menu_tabs($menu_context);

	if (empty($context['error']))
		echo '
					<div class="panel panel-default panel-body clear"><div>
</div>';
}

// Part of the admin layer - used with admin_above to close the table started in it.
function template_generic_menu_dropdown_below()
{
	global $context;

	if (empty($context['error']))
		echo '
		</div>
	</div>';
}

// Some code for showing a tabbed view.
function template_generic_menu_tabs(&$menu_context)
{
	global $context, $settings, $scripturl, $txt, $modSettings;

	// Handy shortcut.
	$tab_context = &$menu_context['tab_data'];

	echo '
					<div class="panel panel-default">';

	if (isset($menu_context['current_area'], $txt[$menu_context['current_area']]) && empty($tab_context['title']))
		$tab_context['title'] = $txt[$menu_context['current_area']];
	if (!empty($tab_context['title']))
	{
		echo '
					<div class="panel-heading">
						<h3 class="panel-title">';

		// Exactly how many tabs do we have?
		if (!empty($context['tabs']))
		{
			foreach ($context['tabs'] as $id => $tab)
			{
				// Can this not be accessed?
				if (!empty($tab['disabled']))
				{
					$tab_context['tabs'][$id]['disabled'] = true;
					continue;
				}

				// Did this not even exist - or do we not have a label?
				if (!isset($tab_context['tabs'][$id]))
					$tab_context['tabs'][$id] = array('label' => $tab['label']);
				elseif (!isset($tab_context['tabs'][$id]['label']))
					$tab_context['tabs'][$id]['label'] = $tab['label'];

				// Has a custom URL defined in the main admin structure?
				if (isset($tab['url']) && !isset($tab_context['tabs'][$id]['url']))
					$tab_context['tabs'][$id]['url'] = $tab['url'];

				// Any additional paramaters for the url?
				if (isset($tab['add_params']) && !isset($tab_context['tabs'][$id]['add_params']))
					$tab_context['tabs'][$id]['add_params'] = $tab['add_params'];

				// Has it been deemed selected?
				if (!empty($tab['is_selected']))
					$tab_context['tabs'][$id]['is_selected'] = true;

				// Does it have its own help?
				if (!empty($tab['help']))
					$tab_context['tabs'][$id]['help'] = $tab['help'];

				// Is this the last one?
				if (!empty($tab['is_last']) && !isset($tab_context['override_last']))
					$tab_context['tabs'][$id]['is_last'] = true;
			}

			// Find the selected tab
			foreach ($tab_context['tabs'] as $sa => $tab)
			{
				if (!empty($tab['is_selected']) || (isset($menu_context['current_subsection']) && $menu_context['current_subsection'] == $sa))
				{
					$selected_tab = $tab;
					$tab_context['tabs'][$sa]['is_selected'] = true;
				}
			}
		}

		// Show an icon and/or a help item?
		if (!empty($selected_tab['icon_class']) || !empty($tab_context['icon_class']) || !empty($selected_tab['icon']) || !empty($tab_context['icon']) || !empty($selected_tab['help']) || !empty($tab_context['help']))
		{
			if (!empty($selected_tab['icon_class']) || !empty($tab_context['icon_class']))
				echo '<span class="', !empty($selected_tab['icon_class']) ? $selected_tab['icon_class'] : $tab_context['icon_class'], ' icon"></span>';
			elseif (!empty($selected_tab['icon']) || !empty($tab_context['icon']))
				echo '<img src="', $settings['images_url'], '/icons/', !empty($selected_tab['icon']) ? $selected_tab['icon'] : $tab_context['icon'], '" alt="" class="icon">';

			if (!empty($selected_tab['help']) || !empty($tab_context['help']))
				echo '<a href="', $scripturl, '?action=helpadmin;help=', !empty($selected_tab['help']) ? $selected_tab['help'] : $tab_context['help'], '" onclick="return reqOverlayDiv(this.href);" class="help"><img src="', $settings['images_url'], '/helptopics_hd.png" alt="', $txt['help'], '" class="icon"></a>';

			echo $tab_context['title'];
		}
		else
		{
			echo '
							', $tab_context['title'];
		}

		echo '
						</h3>
					</div>';
	}

	foreach ($txt as $k => $v)
		if (substr($k, -5) == '_info')
			$txt[strtolower($k)] = $v;

	$desc = '' ;
	if (!empty($selected_tab['description']))
		$desc = $selected_tab['description'];
	elseif (!empty($tab_context['description']))
		$desc = $tab_context['description'];
	elseif (isset($menu_context['current_area'], $txt[$menu_context['current_area'] . '_info']))
		$desc = $txt[$menu_context['current_area'] . '_info'];

	if (!empty($desc))
		echo '
					<p class="panel-body">
						', $desc, '
					</p>';

	echo '
					</div>';

	// Print out all the items in this tab (if any).
	if (!empty($context['tabs']))
	{
		// The admin tabs.
		echo '
						<ul class="nav nav-pills">';

		foreach ($tab_context['tabs'] as $sa => $tab)
		{
			if (!empty($tab['disabled']))
				continue;

			if (!empty($tab['is_selected']))
				echo '
							<li class="active">';
			else
				echo '
							<li>';

			echo '
								<a href="', isset($tab['url']) ? $tab['url'] : $menu_context['base_url'] . ';area=' . $menu_context['current_area'] . ';sa=' . $sa, $menu_context['extra_parameters'], isset($tab['add_params']) ? $tab['add_params'] : '', '">', $tab['label'], '</a>
							</li>';
		}

		// the end of tabs
		echo '
						</ul>';

	}
}

?>