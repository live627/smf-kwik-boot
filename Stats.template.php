<?php
/**
 * Simple Machines Forum (SMF)
 *
 * @package SMF
 * @author Simple Machines
 * @copyright 2014 Simple Machines and individual contributors
 * @license http://www.simplemachines.org/about/smf/license.php BSD
 *
 * @version 2.1 Alpha 1
 */

function template_main()
{
	global $context, $settings, $txt, $scripturl, $modSettings;

	echo '
	<div id="statistics" class="main_section">
			<div class="title_bar">
				<h4 class="titlebg statstitle">
					<span class="stats_icon general"></span>', $txt['general_stats'], '
				</h4>
			</div>
		<div class="flexcontainer">
			<div id="stats_left">
					<table class="full_width" cellpadding="1" cellspacing="0" width="100%">
						<tr>
							<td>', $txt['total_members'], ':</td>
							<td align="right">', $context['show_member_list'] ? '<a href="' . $scripturl . '?action=mlist">' . $context['num_members'] . '</a>' : $context['num_members'], '</td>
						</tr><tr>
							<td>', $txt['total_posts'], ':</td>
							<td align="right">', $context['num_posts'], '</td>
						</tr><tr>
							<td>', $txt['total_topics'], ':</td>
							<td align="right">', $context['num_topics'], '</td>
						</tr><tr>
							<td>', $txt['total_cats'], ':</td>
							<td align="right">', $context['num_categories'], '</td>
						</tr><tr>
							<td>', $txt['users_online'], ':</td>
							<td align="right">', $context['users_online'], '</td>
						</tr><tr>
							<td valign="top">', $txt['most_online'], ':</td>
							<td align="right">', $context['most_members_online']['number'], ' - ', $context['most_members_online']['date'], '</td>
						</tr><tr>
							<td>', $txt['users_online_today'], ':</td>
							<td align="right">', $context['online_today'], '</td>';
	if (!empty($modSettings['hitStats']))
		echo '
						</tr><tr>
							<td>', $txt['num_hits'], ':</td>
							<td align="right">', $context['num_hits'], '</td>';
	echo '
						</tr>
					</table>
			</div>
			<div id="stats_right">
					<table border="0" cellpadding="1" cellspacing="0" width="100%">
						<tr>
							<td>', $txt['average_members'], ':</td>
							<td align="right">', $context['average_members'], '</td>
						</tr><tr>
							<td>', $txt['average_posts'], ':</td>
							<td align="right">', $context['average_posts'], '</td>
						</tr><tr>
							<td>', $txt['average_topics'], ':</td>
							<td align="right">', $context['average_topics'], '</td>
						</tr><tr>
							<td>', $txt['total_boards'], ':</td>
							<td align="right">', $context['num_boards'], '</td>
						</tr><tr>
							<td>', $txt['latest_member'], ':</td>
							<td align="right">', $context['common_stats']['latest_member']['link'], '</td>
						</tr><tr>
							<td>', $txt['average_online'], ':</td>
							<td align="right">', $context['average_online'], '</td>
						</tr><tr>
							<td>', $txt['gender_ratio'], ':</td>
							<td align="right">', $context['gender']['ratio'], '</td>';
	if (!empty($modSettings['hitStats']))
		echo '
						</tr><tr>
							<td>', $txt['average_hits'], ':</td>
							<td align="right">', $context['average_hits'], '</td>';
	echo '
						</tr>
					</table>
			</div>
			<div id="top_posters">
				<div class="title_bar">
					<h4 class="titlebg statstitle">
						<span class="stats_icon posters"></span>', $txt['top_posters'], '
					</h4>
				</div>';

		$pbt = array();
		foreach (array_reverse($context['top_posters']) as $poster)
		{
			$pbt['top_posters']['labels'][] = substr($poster['name'], 0, 20);
			$pbt['top_posters']['data'][] = $poster['post_percent'];
		}

	echo '
	<canvas id="top_posters_canvas" height="300" width="400"></canvas>
			</div>
			<div id="top_boards">
				<div class="title_bar">
					<h4 class="titlebg statstitle">
						<span class="stats_icon boards"></span>', $txt['top_boards'], '
					</h4>
				</div>';

		foreach (array_reverse($context['top_boards']) as $poster)
		{
			$pbt['top_boards']['labels'][] = substr($poster['name'], 0, 20);
			$pbt['top_boards']['data'][] = $poster['post_percent'];
		}

	echo '
	<canvas id="top_boards_canvas" height="300" width="400"></canvas>
			</div>
			<div id="top_topics_replies">
				<div class="title_bar">
					<h4 class="titlebg statstitle">
						<span class="stats_icon replies"></span>', $txt['top_topics_replies'], '
					</h4>
				</div>';

		foreach (array_reverse($context['top_topics_replies']) as $poster)
		{
			$pbt['top_topics_replies']['labels'][] = substr($poster['subject'], 0, 20);
			$pbt['top_topics_replies']['data'][] = $poster['post_percent'];
		}

	echo '
	<canvas id="top_topics_replies_canvas" height="300" width="400"></canvas>
			</div>

			<div id="top_topics_views">
				<div class="title_bar">
					<h4 class="titlebg statstitle">
						<span class="stats_icon views"></span>', $txt['top_topics_views'], '
					</h4>
				</div>';

		foreach (array_reverse($context['top_topics_views']) as $poster)
		{
			$pbt['top_topics_views']['labels'][] = substr($poster['subject'], 0, 20);
			$pbt['top_topics_views']['data'][] = $poster['post_percent'];
		}

	echo '
	<canvas id="top_topics_views_canvas" height="300" width="400"></canvas>
			</div>
			<div id="top_topics_starter">
				<div class="title_bar">
					<h4 class="titlebg statstitle">
						<span class="stats_icon starters"></span>', $txt['top_starters'], '
					</h4>
				</div>';

		foreach (array_reverse($context['top_starters']) as $poster)
		{
			$pbt['top_starters']['labels'][] = substr($poster['name'], 0, 20);
			$pbt['top_starters']['data'][] = $poster['post_percent'];
		}

	echo '
	<canvas id="top_starters_canvas" height="300" width="400"></canvas>
			</div>
			<div id="most_online">
				<div class="title_bar">
					<h4 class="titlebg statstitle">
						<span class="stats_icon history"></span>', $txt['most_time_online'], '
					</h4>
				</div>';

		foreach (array_reverse($context['top_time_online']) as $poster)
		{
			$pbt['top_time_online']['labels'][] = substr($poster['name'], 0, 20);
			$pbt['top_time_online']['data'][] = $poster['time_percent'];
		}

	echo '
	<canvas id="top_time_online_canvas" height="300" width="400"></canvas>
			</div>
		</div>
		<br class="clear" />
		<div class="cat_bar">
			<h3 class="catbg">
				<span class="stats_icon history"></span>', $txt['forum_history'], '
			</h3>
		</div>
		<div>';

	if (!empty($context['yearly']))
	{
		echo '
		<div id="reportrange" class="pull-right">
			<i class="fa fa-calendar fa-lg"></i>
			<span>', date("F j, Y", strtotime('-30 day')), ' - ', date("F j, Y"), '</span> <b class="caret"></b>
		</div>

			<ul class="utility padding" width="900">
				<li>
					<a href="#" id="stats_menu_top" onclick="return false;">', $context['user']['name'], ' &#9660;</a>
					<div id="stats_menu" class="top_menu"></div>
				</li>
			</ul>
	<canvas id="history_canvas" height="400" width="900"></canvas>';

		$y = array();
		$m = array();
		$d = array();
		$col = array(
			'hits' => '200,180,140',
			'new_posts' => '151,187,205',
			'new_topics' => '120,210,180',
			'new_members' => '200,150,180',
			'most_members_online' => '220,160,160',
			'205,151,187',
		);
		$ms = false;
		foreach ($context['yearly'] as $id => $year)
		{
			$y['labels'][] = $id;
			$y['new_topics'][] = $year['new_topics'];
			$y['new_posts'][] = $year['new_posts'];
			$y['new_members'][] = $year['new_members'];
			$y['most_members_online'][] = $year['most_members_online'];

			if (!empty($modSettings['hitStats']))
				$y[$id]['hits'][] = $year['hits'];

			foreach ($year['months'] as $month)
			{
				$m['labels'][] = $month['month'] . ' ' . $month['year'];
				$m['new_topics'][] = $month['new_topics'];
				$m['new_posts'][] = $month['new_posts'];
				$m['new_members'][] = $month['new_members'];
				$m['most_members_online'][] = $month['most_members_online'];

				if (!empty($modSettings['hitStats']))
					$m[$id]['hits'][] = $month['hits'];

				if (!$ms)
				{
					$ms = true;

					foreach ($month['days'] as $day)
					{
						$d['labels'][] = $day['day'];
						$d['new_topics'][] = $day['new_topics'];
						$d['new_posts'][] = $day['new_posts'];
						$d['new_members'][] = $dear['new_members'];
						$d['most_members_online'][] = $day['most_members_online'];

						if (!empty($modSettings['hitStats']))
							$d['hits'][] = $day['hits'];
					}
				}
			}
		}

		echo '
		</div>
	</div>
	<script>
		var
			pbt = ' . json_encode($pbt) .  ',
			colors = ' . json_encode($col) . ',
			txt = ' . json_encode(array(
				'hits' => $txt['page_views'],
				'new_posts' => $txt['stats_new_posts'],
				'new_topics' => $txt['stats_new_topics'],
				'new_members' => $txt['stats_new_members'],
				'most_members_online' => $txt['most_online']
			)) . ',
			datasets = [];
	</script>
	<script id="scr">
		var
			y = ' . json_encode($y) .  ',
			m = ' . json_encode($m) .  ',
			d = ' . json_encode($d) .  ';
	</script>
	<script src="' . $settings['theme_url'] . '/scripts/ChartNew.js"></script>
	<script src="' . $settings['theme_url'] . '/scripts/moment.min.js"></script>
	<script src="' . $settings['theme_url'] . '/scripts/daterangepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="' . $settings['theme_url'] . '/scripts/daterangepicker-bs3.css">
	<script src="' . $settings['theme_url'] . '/scripts/stats.js"></script>';
	}
}

?>