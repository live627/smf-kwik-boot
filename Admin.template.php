<?php
/**
 * Simple Machines Forum (SMF)
 *
 * @package SMF
 * @author Simple Machines http://www.simplemachines.org
 * @copyright 2014 Simple Machines and individual contributors
 * @license http://www.simplemachines.org/about/smf/license.php BSD
 *
 * @version 2.1 Alpha 1
 */

/**
 * This is the administration center home.
 */
function template_Admin_init()
{
	global $settings;

	require_once($settings['default_theme_dir'] . '/Admin.template.php');
}

/**
 * This is the administration center home.
 */
function template_admin_override()
{
	global $context, $settings, $scripturl, $txt, $modSettings;

	// Which menu are we rendering?
	$context['cur_menu_id'] = isset($context['cur_menu_id']) ? $context['cur_menu_id'] : 1;
	$menu_context = &$context['menu_data_' . $context['cur_menu_id']];

	echo '
		<div class="row">
		<div class="col-md-9 panel panel-default">
			<div id="update_section"></div>';

	// Main areas first.
	foreach ($menu_context['sections'] as $section)
	{
		echo '
			<div class="panel-heading">
				<h3 class="panel-title">
					', $section['title'] , '
				</h3>
			</div>
			<ul class="panel-body">';

		// For every area of this section show a link to that area (bold if it's currently selected.)
		foreach ($section['areas'] as $i => $area)
		{
			// Not supposed to be printed?
			if (empty($area['label']))
				continue;

			echo '
				<li>
					', $area['icon'], '<a class="bold_text" href="', (isset($area['url']) ? $area['url'] : $menu_context['base_url'] . ';area=' . $i), $menu_context['extra_parameters'], '">', $area['label'], '</a>';

			if (!empty($area['subsections']))
				foreach ($area['subsections'] as $sa => $sub)
				{
					if (!empty($sub['disabled']))
						continue;

					$url = isset($sub['url']) ? $sub['url'] : (isset($area['url']) ? $area['url'] : $menu_context['base_url'] . ';area=' . $i) . ';sa=' . $sa;

					echo ' - <a href="', $url, $menu_context['extra_parameters'], '">', $sub['label'], '</a>';
				}

			echo '
				</li>';
		}
		echo '
			</ul>';
	}

	// The below functions include all the scripts needed from the simplemachines.org site. The language and format are passed for internationalization.
	if (empty($modSettings['disable_smf_js']))
		echo '
					<script src="', $scripturl, '?action=viewsmfile;filename=current-version.js"></script>
					<script src="', $scripturl, '?action=viewsmfile;filename=latest-news.js"></script>';

	// This sets the announcements and current versions themselves ;).
	echo '
					<script src="', $settings['default_theme_url'], '/scripts/admin.js?alp21"></script>
					<script><!-- // --><![CDATA[
						var oAdminIndex = new smf_AdminIndex({
							sSelf: \'oAdminCenter\',

							bLoadAnnouncements: true,
							sAnnouncementTemplate: ', JavaScriptEscape('
								%content%
							'), ',
							sAnnouncementMessageTemplate: ', JavaScriptEscape('
								<div class="cat_bar"><strong>%subject%</strong><br /><small class="text-muted"> <span class="glyphicon glyphicon-time"></span> %time%</small></div>
								%message%
								<div class="righttext">
									<a href="%href%"><span class="glyphicon glyphicon-comment"></span> Read more...</a>
								</div>
								<br />
							'), ',
							sAnnouncementContainerId: \'smf_news\',

							bLoadVersions: true,
							sSmfVersionContainerId: \'smfVersion\',
							sYourVersionContainerId: \'yourVersion\',
							sVersionOutdatedTemplate: ', JavaScriptEscape('
								<span class="alert">%currentVersion%</span>
							'), ',

							bLoadUpdateNotification: true,
							sUpdateNotificationContainerId: \'update_section\',
							sUpdateNotificationDefaultTitle: ', JavaScriptEscape($txt['update_available']), ',
							sUpdateNotificationDefaultMessage: ', JavaScriptEscape($txt['update_message']), ',
							sUpdateNotificationTemplate: ', JavaScriptEscape('
								<h3 id="update_title">
									%title%
								</h3>
								<div id="update_message" class="smalltext">
									%message%
								</div>
							'), ',
							sUpdateNotificationLink: smf_scripturl + ', JavaScriptEscape('?action=admin;area=packages;pgdownload;auto;package=%package%;' . $context['session_var'] . '=' . $context['session_id']), '

						});
					// ]]></script>
		</div>
			<div class="well well-sm col-md-3 hidden-xs hidden-sm" id="smf_news">
				', $txt['lfyi'], '
			</div>
		</div>';
}

/**
 * Show some support information and credits to those who helped make this.
 */
function template_credits_override()
{
	global $context, $settings, $scripturl, $txt;

	// Show the user version information from their server.
	echo '

					<div id="admincenter">
						<div id="support_credits">
						<div id="support_information" class="col-lg-6">
							<div class="cat_bar">
								<h4>
									', $txt['support_title'], ' <img src="', $settings['images_url'], '/smflogo.png" id="credits_logo" alt="">
								</h4>
							</div>
							<div class="windowbg">
								<div class="content">
									<strong>', $txt['support_versions'], ':</strong><br>
										', $txt['support_versions_forum'], ':
									<em id="yourVersion" style="white-space: nowrap;">', $context['forum_version'], '</em>', $context['can_admin'] ? ' <a href="' . $scripturl . '?action=admin;area=maintain;sa=routine;activity=version">' . $txt['version_check_more'] . '</a>' : '', '<br>
										', $txt['support_versions_current'], ':
									<em id="smfVersion" style="white-space: nowrap;">??</em><br>';

	// Display all the variables we have server information for.
	foreach ($context['current_versions'] as $version)
	{
		echo '
										', $version['title'], ':
									<em>', $version['version'], '</em>';

		// more details for this item, show them a link
		if ($context['can_admin'] && isset($version['more']))
			echo
									' <a href="', $scripturl, $version['more'], ';', $context['session_var'], '=', $context['session_id'], '">', $txt['version_check_more'], '</a>';
		echo '
									<br>';
	}

	echo '
								</div>
							</div>
							</div>';

	// Point the admin to common support resources.
	echo '
							<div id="support_resources" class="col-lg-6">
							<div class="cat_bar">
								<h4>
									', $txt['support_resources'], '
								</h4>
							</div>
							<div class="windowbg2">
								<div class="content">
									<p>', $txt['support_resources_p1'], '</p>
									<p>', $txt['support_resources_p2'], '</p>
								</div>
							</div>
							</div>';

	// The most important part - the credits :P.
	echo '
							<div class="cat_bar">
								<h4>
									', $txt['admin_credits'], '
								</h4>
							</div>
							<div class="windowbg2">
								<div class="content">';

	foreach ($context['credits'] as $section)
	{
		if (isset($section['pretext']))
			echo '
									<p>', $section['pretext'], '</p><hr>';

		echo '
									<dl class="dl-horizontal">';

		foreach ($section['groups'] as $id => $group)
		{
			if (isset($group['title']))
				echo '
										<dt>
											', $group['title'], '
										</dt>';

			echo '
										<dd>', implode(', ', $group['members']), '</dd>';
		}

		echo '
									</dl>';

		if (isset($section['posttext']))
			echo '
									<hr class="clear">
									<p>', $section['posttext'], '</p>';
	}

	echo '
								</div>
							</div>
						</div>
					</div>';

	// This makes all the support information available to the support script...
	echo '
						<script><!-- // --><![CDATA[
							var smfSupportVersions = {};

							smfSupportVersions.forum = "', $context['forum_version'], '";';

	// Don't worry, none of this is logged, it's just used to give information that might be of use.
	foreach ($context['current_versions'] as $variable => $version)
		echo '
							smfSupportVersions.', $variable, ' = "', $version['version'], '";';

	// Now we just have to include the script and wait ;).
	echo '
						// ]]></script>
						<script src="', $scripturl, '?action=viewsmfile;filename=current-version.js"></script>
						<script src="', $scripturl, '?action=viewsmfile;filename=latest-news.js"></script>';

	// This sets the latest support stuff.
	echo '
						<script><!-- // --><![CDATA[
							function smfCurrentVersion()
							{
								var smfVer, yourVer;

								if (!window.smfVersion)
									return;

								smfVer = document.getElementById("smfVersion");
								yourVer = document.getElementById("yourVersion");

								setInnerHTML(smfVer, window.smfVersion);

								var currentVersion = getInnerHTML(yourVer);
								if (currentVersion != window.smfVersion)
									setInnerHTML(yourVer, "<span class=\"alert\">" + currentVersion + "</span>");
							}
							addLoadEvent(smfCurrentVersion)
						// ]]></script>';
}

// Template for showing settings (Of any kind really!)
function template_show_settings_override()
{
	global $context, $txt, $settings, $scripturl;

	if (!empty($context['saved_successful']))
		echo '
					<div class="infobox">', $txt['settings_saved'], '</div>';
	elseif (!empty($context['saved_failed']))
		echo '
					<div class="errorbox">', sprintf($txt['settings_not_saved'], $context['saved_failed']), '</div>';

	if (!empty($context['settings_pre_javascript']))
		echo '
					<script><!-- // --><![CDATA[', $context['settings_pre_javascript'], '// ]]></script>';

	if (!empty($context['settings_insert_above']))
		echo $context['settings_insert_above'];

	echo '
					<div id="admincenter">
						<form id="admin_form_wrapper" action="', $context['post_url'], '" method="post" accept-charset="', $context['character_set'], '"', !empty($context['force_form_onsubmit']) ? ' onsubmit="' . $context['force_form_onsubmit'] . '"' : '', '>';

	// Is there a custom title?
	if (isset($context['settings_title']))
		echo '
							<div class="cat_bar">
								<h3 class="catbg">
									', $context['settings_title'], '
								</h3>
							</div>';

	// Have we got a message to display?
	if (!empty($context['settings_message']))
		echo '
							<div class="information">', $context['settings_message'], '</div>';

	// Now actually loop through all the variables.
	$is_open = false;
	foreach ($context['config_vars'] as $config_var)
	{
		// Is it a title or a description?
		if (is_array($config_var) && ($config_var['type'] == 'title' || $config_var['type'] == 'desc'))
		{
			// Not a list yet?
			if ($is_open)
			{
				$is_open = false;
				echo '
									</dl>
							</div>';
			}

			// A title?
			if ($config_var['type'] == 'title')
			{
				echo '
							<div class="cat_bar">
								<h3 class="', !empty($config_var['class']) ? $config_var['class'] : 'catbg', '"', !empty($config_var['force_div_id']) ? ' id="' . $config_var['force_div_id'] . '"' : '', '>
									', ($config_var['help'] ? '<a href="' . $scripturl . '?action=helpadmin;help=' . $config_var['help'] . '" onclick="return reqOverlayDiv(this.href);" class="help"><span class="generic_icons help" title="'. $txt['help'].'"></span></a>' : ''), '
									', $config_var['label'], '
								</h3>
							</div>';
			}
			// A description?
			else
			{
				echo '
							<p class="information">
								', $config_var['label'], '
							</p>';
			}

			continue;
		}

		// Not a list yet?
		if (!$is_open)
		{
			$is_open = true;
			echo '
							<div class="windowbg2">
								<dl class="settings">';
		}

		// Hang about? Are you pulling my leg - a callback?!
		if (is_array($config_var) && $config_var['type'] == 'callback')
		{
			if (function_exists('template_callback_' . $config_var['name']))
				call_user_func('template_callback_' . $config_var['name']);

			continue;
		}

		if (is_array($config_var))
		{
			// First off, is this a span like a message?
			if (in_array($config_var['type'], array('message', 'warning')))
			{
				echo '
									<dd', $config_var['type'] == 'warning' ? ' class="alert"' : '', (!empty($config_var['force_div_id']) ? ' id="' . $config_var['force_div_id'] . '_dd"' : ''), '>
										', $config_var['label'], '
									</dd>';
			}
			// Otherwise it's an input box of some kind.
			else
			{
				echo '
									<dt', is_array($config_var) && !empty($config_var['force_div_id']) ? ' id="' . $config_var['force_div_id'] . '"' : '', '>';

				// Some quick helpers...
				$javascript = $config_var['javascript'];
				$disabled = !empty($config_var['disabled']) ? ' disabled' : '';
				$subtext = !empty($config_var['subtext']) ? '<br><span class="smalltext"> ' . $config_var['subtext'] . '</span>' : '';

				// Various HTML5 input types that are basically enhanced textboxes
				$text_types = array('color', 'date', 'datetime', 'datetime-local', 'email', 'month', 'time');

				// Show the [?] button.
				if ($config_var['help'])
					echo '
							<a id="setting_', $config_var['name'], '" href="', $scripturl, '?action=helpadmin;help=', $config_var['help'], '" onclick="return reqWin(this.href);"><span class="generic_icons help" title="', $txt['help'],'"></span></a> ';

				echo '
										<a id="setting_', $config_var['name'], '"></a> <span', ($config_var['disabled'] ? ' style="color: #777777;"' : ($config_var['invalid'] ? ' class="error"' : '')), '><label for="', $config_var['name'], '">', $config_var['label'], '</label>', $subtext, ($config_var['type'] == 'password' ? '<br><em>' . $txt['admin_confirm_password'] . '</em>' : ''), '</span>
									</dt>
									<dd', (!empty($config_var['force_div_id']) ? ' id="' . $config_var['force_div_id'] . '_dd"' : ''), '>',
										$config_var['preinput'];

				// Show a check box.
				if ($config_var['type'] == 'check')
					echo '
										<input type="checkbox"', $javascript, $disabled, ' name="', $config_var['name'], '" id="', $config_var['name'], '"', ($config_var['value'] ? ' checked' : ''), ' value="1" class="input_check">';
				// Escape (via htmlspecialchars.) the text box.
				elseif ($config_var['type'] == 'password')
					echo '
										<input type="password"', $disabled, $javascript, ' name="', $config_var['name'], '[0]"', ($config_var['size'] ? ' size="' . $config_var['size'] . '"' : ''), ' value="*#fakepass#*" onfocus="this.value = \'\'; this.form.', $config_var['name'], '.disabled = false;" class="input_password"><br>
										<input type="password" disabled id="', $config_var['name'], '" name="', $config_var['name'], '[1]"', ($config_var['size'] ? ' size="' . $config_var['size'] . '"' : ''), ' class="input_password">';
				// Show a selection box.
				elseif ($config_var['type'] == 'select')
				{
					echo '
										<select name="', $config_var['name'], '" id="', $config_var['name'], '" ', $javascript, $disabled, (!empty($config_var['multiple']) ? ' multiple="multiple"' : ''), '>';
					foreach ($config_var['data'] as $option)
						echo '
											<option value="', $option[0], '"', (!empty($config_var['value']) && ($option[0] == $config_var['value'] || (!empty($config_var['multiple']) && in_array($option[0], $config_var['value']))) ? ' selected' : ''), '>', $option[1], '</option>';
					echo '
										</select>';
				}
				// List of boards? This requires getBoardList() having been run and the results in $context['board_list'].
				elseif ($config_var['type'] == 'boards')
				{
					$board_list = true;
					$first = true;
					echo '
										<a href="#" class="board_selector">[ ', $txt['select_boards_from_list'], ' ]</a>
										<fieldset>
												<legend class="board_selector"><a href="#">', $txt['select_boards_from_list'], '</a></legend>';
					foreach ($context['board_list'] as $id_cat => $cat)
					{
						if (!$first)
							echo '
											<hr>';
						echo '
											<strong>', $cat['name'], '</strong>
											<ul>';
						foreach ($cat['boards'] as $id_board => $brd)
							echo '
												<li><label><input type="checkbox" name="', $config_var['name'], '[', $brd['id'], ']" value="1" class="input_check"', in_array($brd['id'], $config_var['value']) ? ' checked' : '', '> ', $brd['child_level'] > 0 ? str_repeat('&nbsp; &nbsp;', $brd['child_level']) : '', $brd['name'], '</label></li>';

						echo '
											</ul>';
						$first = false;
					}
					echo '
											</fieldset>';
				}
				// Text area?
				elseif ($config_var['type'] == 'large_text')
					echo '
											<textarea rows="', (!empty($config_var['size']) ? $config_var['size'] : (!empty($config_var['rows']) ? $config_var['rows'] : 4)), '" cols="', (!empty($config_var['cols']) ? $config_var['cols'] : 30), '" ', $javascript, $disabled, ' name="', $config_var['name'], '" id="', $config_var['name'], '">', $config_var['value'], '</textarea>';
				// Permission group?
				elseif ($config_var['type'] == 'permissions')
					theme_inline_permissions($config_var['name']);
				// BBC selection?
				elseif ($config_var['type'] == 'bbc')
				{
					echo '
											<fieldset id="', $config_var['name'], '">
												<legend>', $txt['bbcTagsToUse_select'], '</legend>
													<ul class="reset">';

					foreach ($context['bbc_columns'] as $bbcColumn)
					{
						foreach ($bbcColumn as $bbcTag)
							echo '
														<li class="list_bbc floatleft">
															<input type="checkbox" name="', $config_var['name'], '_enabledTags[]" id="tag_', $config_var['name'], '_', $bbcTag['tag'], '" value="', $bbcTag['tag'], '"', !in_array($bbcTag['tag'], $context['bbc_sections'][$config_var['name']]['disabled']) ? ' checked' : '', ' class="input_check"> <label for="tag_', $config_var['name'], '_', $bbcTag['tag'], '">', $bbcTag['tag'], '</label>', $bbcTag['show_help'] ? ' (<a href="' . $scripturl . '?action=helpadmin;help=tag_' . $bbcTag['tag'] . '" onclick="return reqOverlayDiv(this.href);">?</a>)' : '', '
														</li>';
					}
					echo '							</ul>
												<input type="checkbox" id="bbc_', $config_var['name'], '_select_all" onclick="invertAll(this, this.form, \'', $config_var['name'], '_enabledTags\');"', $context['bbc_sections'][$config_var['name']]['all_selected'] ? ' checked' : '', ' class="input_check"> <label for="bbc_', $config_var['name'], '_select_all"><em>', $txt['bbcTagsToUse_select_all'], '</em></label>
											</fieldset>';
				}
				// A simple message?
				elseif ($config_var['type'] == 'var_message')
					echo '
											<div', !empty($config_var['name']) ? ' id="' . $config_var['name'] . '"' : '', '>', $config_var['var_message'], '</div>';
				// Assume it must be a text box
				else
				{
					// Figure out the exact type - use "number" for "float" and "int".
					$type = in_array($config_var['type'], $text_types) ? $config_var['type'] : ($config_var['type'] == 'int' || $config_var['type'] == 'float' ? 'number' : 'text');

					// Extra options for float/int values - how much to decrease/increase by, the min value and the max value
					// The step - only set if incrementing by something other than 1 for int or 0.1 for float
					$step = isset($config_var['step']) ? ' step="' . $config_var['step'] . '"' : ($config_var['type'] == 'float' ? ' step="0.1"' : '');

					// Minimum allowed value for this setting. SMF forces a default of 0 if not specified in the settings
					$min = isset($config_var['min']) ? ' min="' . $config_var['min'] . '"' : '';

					// Maximum allowed value for this setting.
					$max = isset($config_var['max']) ? ' max="' . $config_var['max'] . '"' : '';

					echo '
											<input type="', $type ,'"', $javascript, $disabled, ' name="', $config_var['name'], '" id="', $config_var['name'], '" value="', $config_var['value'], '"', ($config_var['size'] ? ' size="' . $config_var['size'] . '"' : ''), ' class="input_text"', $min . $max . $step, '>';
				}

				echo isset($config_var['postinput']) ? '
											' . $config_var['postinput'] : '',
										'</dd>';
			}
		}

		else
		{
			// Just show a separator.
			if ($config_var == '')
				echo '
								</dl>
								<hr class="hrcolor clear">
								<dl class="settings">';
			else
				echo '
									<dd>
										<strong>' . $config_var . '</strong>
									</dd>';
		}
	}

	if ($is_open)
		echo '
								</dl>';

	if (empty($context['settings_save_dont_show']))
		echo '
								<input type="submit" value="', $txt['save'], '"', (!empty($context['save_disabled']) ? ' disabled' : ''), (!empty($context['settings_save_onclick']) ? ' onclick="' . $context['settings_save_onclick'] . '"' : ''), ' class="button_submit">';

	if ($is_open)
		echo '
							</div>';


	// At least one token has to be used!
	if (isset($context['admin-ssc_token']))
		echo '
							<input type="hidden" name="', $context['admin-ssc_token_var'], '" value="', $context['admin-ssc_token'], '">';

	if (isset($context['admin-dbsc_token']))
		echo '
							<input type="hidden" name="', $context['admin-dbsc_token_var'], '" value="', $context['admin-dbsc_token'], '">';

	if (isset($context['admin-mp_token']))
		echo '
							<input type="hidden" name="', $context['admin-mp_token_var'], '" value="', $context['admin-mp_token'], '">';

	echo '
							<input type="hidden" name="', $context['session_var'], '" value="', $context['session_id'], '">
						</form>
					</div>';

	if (!empty($context['settings_post_javascript']))
		echo '
					<script><!-- // --><![CDATA[
					', $context['settings_post_javascript'], '
					// ]]></script>';

	if (!empty($context['settings_insert_below']))
		echo $context['settings_insert_below'];

	// We may have added a board listing. If we did, we need to make it work.
	addInlineJavascript('
	$("legend.board_selector").closest("fieldset").hide();
	$("a.board_selector").click(function(e) {
		e.preventDefault();
		$(this).hide().next("fieldset").show();
	});
	$("fieldset legend.board_selector a").click(function(e) {
		e.preventDefault();
		$(this).closest("fieldset").hide().prev("a").show();
	});
	', true);
}

?>