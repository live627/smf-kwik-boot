<?php
/**
 * Simple Machines Forum (SMF)
 *
 * @package SMF
 * @author Simple Machines
 * @copyright 2014 Simple Machines and individual contributors
 * @license http://www.simplemachines.org/about/smf/license.php BSD
 *
 * @version 2.1 Alpha 1
 */

/*	This template is, perhaps, the most important template in the theme. It
	contains the main template layer that displays the header and footer of
	the forum, namely with main_above and main_below. It also contains the
	menu sub template, which appropriately displays the menu; the init sub
	template, which is there to set the theme up; (init can be missing.) and
	the linktree sub template, which sorts out the link tree.

	The init sub template should load any data and set any hardcoded options.

	The main_above sub template is what is shown above the main content, and
	should contain anything that should be shown up there.

	The main_below sub template, conversely, is shown after the main content.
	It should probably contain the copyright statement and some other things.

	The linktree sub template should display the link tree, using the data
	in the $context['linktree'] variable.

	The menu sub template should display all the relevant buttons the user
	wants and or needs.

	For more information on the templating system, please see the site at:
	http://www.simplemachines.org/
*/

/**
 * Initialize the template... mainly little settings.
 */
function template_init()
{
	global $context, $modSettings, $scripturl, $settings, $txt;

	/* $context, $options and $txt may be available for use, but may not be fully populated yet. */

	/* Use images from default theme when using templates from the default theme?
		if this is 'always', images from the default theme will be used.
		if this is 'defaults', images from the default theme will only be used with default templates.
		if this is 'never' or isn't set at all, images from the default theme will not be used. */
	$settings['use_default_images'] = 'never';

	// The version this template/theme is for. This should probably be the version of SMF it was created for.
	$settings['theme_version'] = '2.1';

	// Use plain buttons - as opposed to text buttons?
	$settings['use_buttons'] = true;

	// Show sticky and lock status separate from topic icons?
	$settings['separate_sticky_lock'] = true;

	// Set the following variable to true if this theme requires the optional theme strings file to be loaded.
	$settings['require_theme_strings'] = true;

	// Set the following variable to true is this theme wants to display the avatar of the user that posted the last post on the board index and message index
	$settings['avatars_on_indexes'] = false;

	if (isset($_GET['action']))
	{
		$settings['catch_action'] = array(
			//'template' => 'Custom',
			'filename' => '$themedir/custom/' . ucfirst($_GET['action']) . '.php',
			'function' => ucfirst($_GET['action']),
			'template' => 'custom/' . ucfirst($_GET['action']),
			'sub_template' => $_GET['action'],
		);
	}

	//$settings['theme_variants'] = array('brave', 'clean', 'darkly', 'flatly', 'gunmetal', 'sleek', 'wine');
	//$settings['default_variant'] = 'clean';

	$settings['page_index'] = array(
		'extra_before' => '<ul class="pagination">',
		'previous_page' => '&laquo;',
		'current_page' => '<li class="active"><span>%1$d <span class="sr-only"></span></span></li> ',
		'page' => '<li><a href="{URL}">%2$s</a></li> ',
		'expand_pages' => '<li><span onclick="expandPages(this, {LINK}, {FIRST_PAGE}, {LAST_PAGE}, {PER_PAGE});"> ... </span></li>',
		'next_page' => '&raquo;',
		'extra_after' => '</ul>',
	);
	addInlineJavascript('
	var user_menus = new smc_PopupMenu();
	user_menus.add("search", smf_prepareScriptUrl(smf_scripturl) + "action=searchpop;board=' . (isset($context['current_board']) ? $context['current_board'] : '') . ';topic=' . (isset($context['current_topic']) ? $context['current_topic'] : '') . '");', true);

	add_integration_function('integrate_menu_buttons', 'changeButtons', false, '$themedir/custom/fn.php', false);
	if (!empty($modSettings['integrate_menu_buttons']))
	{
		$current_functions = explode(',', $modSettings['integrate_menu_buttons']);
		if (!in_array('fixSubTemplates', $current_functions))
			add_integration_function('integrate_menu_buttons', 'fixSubTemplates', true, '$themedir/custom/fn.php');
	}
	add_integration_function('integrate_buffer', 'fixMarkup', false, '$themedir/custom/fn.php', false);
	add_integration_function('integrate_pre_javascript_output', 'fixJS', false, '$themedir/custom/fn.php', false);
	add_integration_function('integrate_pre_css_output', 'fixCSS', false, '$themedir/custom/fn.php', false);
	add_integration_function('integrate_actions', 'members_ads_integrate_actions', false, '$themedir/custom/fn.php', false);
	add_integration_function('integrate_output_error', 'fixErrors', false, '$themedir/custom/fn.php', false);
	add_integration_function('integrate_bbc_codes', 'fixBBC', false, '$themedir/custom/fn.php', false);
	add_integration_function('integrate_default_action', 'default_action', false, '$themedir/custom/fn.php', false);
	loadCSSFile('bootstrap.min.css', array('nolog' => true));
	loadCSSFile('flexbox-grid.min.css', array('nolog' => true));
	loadCSSFile('bootstrap-select.min.css', array('nolog' => true));
	loadCSSFile('bbc.css', array('nolog' => true));
	loadCSSFile('menu.css', array('nolog' => true));
	loadCSSFile('wrapper.css', array('nolog' => true));
	loadCSSFile('reseller.css', array('nolog' => true));
	loadCSSFile('layout.css', array('nolog' => true));
	loadCSSFile('forms.css', array('nolog' => true));
	loadCSSFile('bi.css', array('nolog' => true));
	loadCSSFile('prettify.css', array('nolog' => true));
	loadCSSFile('tabdrop.css', array('nolog' => true));
	loadCSSFile('jquery.sb.css', array('nolog' => true));
	loadJavascriptFile('bootstrap.min.js', array('nolog' => true));
	loadJavascriptFile('prettify.min.js', array('nolog' => true));
	loadJavascriptFile('bootstrap-tabdrop.min.js', array('nolog' => true));
	loadJavascriptFile('bootstrap-select.js', array('nolog' => true));
	loadJavascriptFile('bootstrap-maxlength.min.js', array('nolog' => true));

	// Is this a page requested through jQuery/XHR? If so, set the AJAX constant so we can choose to show only the template's default block.
	define('AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

	global $scripturl, $smcFunc, $user_info;

	$request = $smcFunc['db_query']('', '
		SELECT id_group, group_name
		FROM {db_prefix}membergroups
		WHERE id_group = {int:id_group}',
		array(
			'id_group' => $user_info['groups'][0]
		)
	);
	while ($row = $smcFunc['db_fetch_assoc']($request))
		 $context['user']['group'] = '<a href="' . $scripturl . '?action=groups;sa=members;group=' . $row['id_group'] . '">' . $row['group_name'] . '</a>';
	$smcFunc['db_free_result']($request);

	loadLanguage('Alerts');
}

/**
 * The main sub template above the content.
 */
function template_html_above()
{
	global $context, $settings, $scripturl, $txt, $modSettings;

	// Show right to left and the character set for ease of translating.
	echo '<!DOCTYPE html>
<html', $context['right_to_left'] ? ' dir="rtl"' : '', '>
<head>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>';/*';*/

	// Special case: get all included files and strip their paths.
	$included_files = get_included_files();

	foreach ($included_files as $filename)
		$files[] = basename($filename);

	// We have an addditional stylesheet for the board index.
	if (in_array('BoardIndex.php', $files) || in_array('MessageIndex.php', $files))
		loadCSSFile('bi.css');

	// We also have an addditional stylesheet for the topic display.
	if (in_array('Display.php', $files))
		loadCSSFile('d.css');

	if (in_array('Calendar.php', $files))
		loadCSSFile('c.css');

	//loadCSSFile($context['theme_variant'] . '/bootstrap.css');

	// load in any css from mods or themes so they can overwrite if wanted
	template_css();

	// Save some database hits, if a width for multiple wrappers is set in admin.
	if (!empty($settings['forum_width']))
		echo '
	<style type="text/css">.container {width: ', $settings['forum_width'], '; margin: 0 auto;}</style>';

	// load in any javascript files from mods and themes
	template_javascript();

	echo '
	<meta http-equiv="Content-Type" content="text/html; charset=', $context['character_set'], '" />
	<meta name="description" content="', !empty($context['meta_description']) ? $context['meta_description'] : $context['page_title_html_safe'], '" />', !empty($context['meta_keywords']) ? '
	<meta name="keywords" content="' . $context['meta_keywords'] . '" />' : '', '
	<title>', $context['page_title_html_safe'], '</title>';

	// Please don't index these Mr Robot.
	if (!empty($context['robot_no_index']))
		echo '
	<meta name="robots" content="noindex" />';

	// Present a canonical url for search engines to prevent duplicate content in their indices.
	if (!empty($context['canonical_url']))
		echo '
	<link rel="canonical" href="', $context['canonical_url'], '" />';

	// Show all the relative links, such as help, search, contents, and the like.
	echo '
	<link rel="help" href="', $scripturl, '?action=help" />
	<link rel="contents" href="', $scripturl, '" />', ($context['allow_search'] ? '
	<link rel="search" href="' . $scripturl . '?action=search" />' : '');

	// If RSS feeds are enabled, advertise the presence of one.
	if (!empty($modSettings['xmlnews_enable']) && (!empty($modSettings['allow_guestAccess']) || $context['user']['is_logged']))
		echo '
	<link rel="alternate" type="application/rss+xml" title="', $context['forum_name_html_safe'], ' - ', $txt['rss'], '" href="', $scripturl, '?type=rss2;action=.xml" />
	<link rel="alternate" type="application/rss+xml" title="', $context['forum_name_html_safe'], ' - ', $txt['atom'], '" href="', $scripturl, '?type=atom;action=.xml" />';

	// If we're viewing a topic, these should be the previous and next topics, respectively.
	if (!empty($context['links']['next']))
	{
		echo '
	<link rel="next" href="', $context['links']['next'], '" />';
	}

	if (!empty($context['links']['prev']))
	{
		echo '
	<link rel="prev" href="', $context['links']['prev'], '" />';
	}

	// If we're in a board, or a topic for that matter, the index will be the board's index.
	if (!empty($context['current_board']))
		echo '
	<link rel="index" href="', $scripturl, '?board=', $context['current_board'], '.0" />';

	// Output any remaining HTML headers. (from mods, maybe?)
	echo $context['html_headers'];

	echo '
</head>
<body id="', $context['browser_body_id'], '" class="action_', !empty($context['current_action']) ? $context['current_action'] : (!empty($context['current_board']) ?
		'messageindex' : (!empty($context['current_topic']) ? 'display' : 'home')), !empty($context['current_board']) ? ' board_' . $context['current_board'] : '', '">';
}

function template_body_above()
{
	global $context, $settings, $scripturl, $txt, $modSettings;

	echo'
<nav class="navbar navbar-default navbar-static-top" role="navigation">
	<div class="container">
		<div class="row">
			<div class="navbar-header visible-xs">
				<a class="navbar-brand" href="', $scripturl, '">', $context['forum_name'] ,'</a>
				<a href="#" class="navbar-toggle floatright" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<i class="fa fa-bars"></i>
				</a>
			</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<div>
				<ul class="nav navbar-nav">';

	// Show the menu here, according to the menu sub template.
	template_menu();

	echo'
				</ul>
			</div>
			</div>
		</div>
	</div>
</nav>
<header>
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<h1><a href="', $scripturl, '">', $context['page_title_html_safe'], '</a></h1>
			</div>
			<div class="col-md-3 visible-xs visible-sm">';

	if (!$context['user']['is_logged'])
		echo'
				<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">', $txt['login'] ,'</button>
				<button type="button" class="btn btn-primary" onclick="location.href=\''. $scripturl .'?action=signup\'">', $txt['register'] ,'</button>
				<form id="guest_form" action="', $scripturl, '?action=login2" method="post" accept-charset="', $context['character_set'], '">
					<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog modal-sm">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
									<h4 class="modal-title" id="myModalLabel">', $txt['login'] ,'</h4>
								</div>
								<div class="modal-body">
										<div class="form-group">
											<input type="text" name="user" class="form-control" placeholder="', $txt['user'] ,'" />
										</div>
										<div class="form-group">
											<input type="password" name="passwrd" class="form-control" placeholder="', $txt['password'] ,'" />
										</div>
										<div class="checkbox">
											<label>
												<input name="cookielength" type="checkbox" value="-1" /> Remember me
											</label>
										</div>
								</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-success">', $txt['login'] ,'</button>
								</div>
								<input type="hidden" name="', $context['session_var'], '" value="', $context['session_id'], '">
								<input type="hidden" name="', $context['login_token_var'], '" value="', $context['login_token'], '">
							</div>
						</div>
					</div>
				</form>';

	echo'
			</div>
		</div>
	</div>
</header>';

	// Show the navigation tree.
	theme_linktree();

echo'
<div class="container">';

		return;
	echo '
	<div class="audience-switcher">
		<ul>
			<li><a class="here">SMF</a></li>
			<li><a href="here">Wedge</a></li>';

	if ($context['allow_search'])
	{
		echo '
			<li class="floatright"><form id="search_form" action="', $scripturl, '?action=search2" method="post" accept-charset="', $context['character_set'], '">
				<input type="text" name="search" value="" class="input_text" id="search_menu_top" />&nbsp;';

			$selected = !empty($context['current_topic']) ? 'current_topic' : (!empty($context['current_board']) ? 'current_board' : 'all');

			echo '
				<select name="search_selection">
					<option value="all"', ($selected == 'all' ? ' selected="selected"' : ''), '>', $txt['search_entireforum'], ' </option>';

			// Can't limit it to a specific topic if we are not in one
			if (!empty($context['current_topic']))
				echo '
					<option value="topic"', ($selected == 'current_topic' ? ' selected="selected"' : ''), '>', $txt['search_thistopic'], '</option>';

		// Can't limit it to a specific board if we are not in one
		if (!empty($context['current_board']))
			echo '
					<option value="board"', ($selected == 'current_board' ? ' selected="selected"' : ''), '>', $txt['search_thisbrd'], '</option>';
			echo '
					<option value="members"', ($selected == 'members' ? ' selected="selected"' : ''), '>', $txt['search_members'], ' </option>
				</select>';

		// Search within current topic?
		if (!empty($context['current_topic']))
			echo '
				<input type="hidden" name="', (!empty($modSettings['search_dropdown']) ? 'sd_topic' : 'topic'), '" value="', $context['current_topic'], '" />';
		// If we're on a certain board, limit it to this board ;).
		elseif (!empty($context['current_board']))
			echo '
				<input type="hidden" name="', (!empty($modSettings['search_dropdown']) ? 'sd_brd[' : 'brd['), $context['current_board'], ']"', ' value="', $context['current_board'], '" />';

		echo '
				<input type="submit" name="search2" value="', $txt['search'], '" class="button_submit" />
				<input type="hidden" name="advanced" value="0" />
			</form>
					<div id="search_menu" class="top_menu"></div>
		</li>';
	}

	echo '
		</ul>
	</div>	<!--	END audience-switcher -->

	<div id="section1">
		<div id="masthead">
			<h1 class="forumtitle">
				<a id="top" href="', $scripturl, '">', empty($context['header_logo_url_html_safe']) ? $context['forum_name'] : '<img src="' . $context['header_logo_url_html_safe'] . '" alt="' . $context['forum_name'] . '" />', '</a>
			</h1>';

	if ($context['user']['is_guest'])
	{
		echo '
						<script src="', $settings['default_theme_url'], '/scripts/sha1.js"></script>
						<form id="guest_form" class="floatright righttext" action="', $scripturl, '?action=login2;quicklogin" method="post" accept-charset="', $context['character_set'], '" ', empty($context['disable_login_hashing']) ? ' onsubmit="hashLoginPassword(this, \'' . $context['session_id'] . '\', \'' . (!empty($context['login_token']) ? $context['login_token'] : '') . '\');"' : '', '>
							<input type="text" name="user" size="10" class="input_text" />
							<input type="password" name="passwrd" size="10" class="input_password" />
							<select name="cookielength">
								<option value="60">', $txt['one_hour'], '</option>
								<option value="1440">', $txt['one_day'], '</option>
								<option value="10080">', $txt['one_week'], '</option>
								<option value="43200">', $txt['one_month'], '</option>
								<option value="-1" selected="selected">', $txt['forever'], '</option>
							</select>
							<input type="submit" value="', $txt['login'], '" class="button_submit" />
							<div>', $txt['quick_login_dec'], '</div>';

		if (!empty($modSettings['enableOpenID']))
			echo '
							<br /><input type="text" name="openid_identifier" size="25" class="input_text openid_login" />';

		echo '
							<input type="hidden" name="hash_passwrd" value="" />
							<input type="hidden" name="', $context['session_var'], '" value="', $context['session_id'], '" />
							<input type="hidden" name="', $context['login_token_var'], '" value="', $context['login_token'], '" />
						</form>';
	}

	echo'
			<div class="utility floatright">
				<ul>';

	if ($context['user']['is_logged'])
	{
		echo '
				<li>
					<a href="', $scripturl, '?action=profile"', !empty($context['self_profile']) ? ' class="active"' : '', ' id="profile_menu_top" onclick="return false;">', $context['user']['name'], ' &#9660;</a>
					<div id="profile_menu" class="top_menu"></div>
				</li>';

		if ($context['allow_pm'])
			echo '
				<li>
					<a href="', $scripturl, '?action=pm"', !empty($context['self_pm']) ? ' class="active"' : '', ' id="pm_menu_top">', $txt['pm_short'], !empty($context['user']['unread_messages']) ? ' <span class="amt">' . $context['user']['unread_messages'] . '</span>' : '', '</a>
					<div id="pm_menu" class="top_menu"></div>
				</li>';

		echo '
				<li>
					<a href="', $scripturl, '?action=alerts"', !empty($context['self_alerts']) ? ' class="active"' : '', ' id="alerts_menu_top">', $txt['alerts'], !empty($context['user']['alerts']) ? ' <span class="amt">' . $context['user']['alerts'] . '</span>' : '', '</a>
					<div id="alerts_menu" class="top_menu"></div>
				</li>
					<li>
						<a href="', $scripturl, '?action=unread" title="', $txt['unread_since_visit'], '">', $txt['view_unread_category'], '</a>
					</li>
					<li>
						<a href="', $scripturl, '?action=unreadreplies" title="', $txt['show_unread_replies'], '">', $txt['unread_replies'], '</a>
					</li>';
	}

	echo '
				</ul>';
	// Show a random news item? (or you could pick one from news_lines...)
	if (!empty($settings['enable_news']) && !empty($context['random_news_line']))
		echo '
					<div class="news floatright righttext">
						<strong>', $txt['news'], ': </strong>
						<p>', $context['random_news_line'], '</p>
					</div>';

	echo '
			</div>';

	template_menu();

	echo '
		</div>
	</div>

	<div id="wrapper">
		<div id="content">
		<div class=errorbox>heyrtyhjy e eryuyu wtryuh rtyw ytrtyrey eterwttyrwtytqywy yyrtrtwytryhg</div>
		<div class=noticebox>heyrtyhjy e eryuyu wtryuh rtyw ytrtyrey eterwttyrwtytqywy yyrtrtwytryhg</div>
		<div class=infobox>heyrtyhjy e eryuyu wtryuh rtyw ytrtyrey eterwttyrwtytqywy yyrtrtwytryhg</div>
		<div class=descbox>heyrtyhjy e eryuyu wtryuh rtyw ytrtyrey eterwttyrwtytqywy yyrtrtwytryhg</div>
		';

	// Show the menu here, according to the menu sub template, followed by the navigation tree.

	theme_linktree();
}

function template_body_below()
{
	global $context, $scripturl, $txt;

	echo '
		</div>
	</div>
</div>';

		// Show the "Powered by" and "Valid" logos, as well as the copyright. Remember, the copyright must be somewhere!
		echo '
		<footer>
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
			<a href="#top_section" id="bot" class="go_up">', $txt['go_up'], '</a>
			<ul class="floatright">
				<li><a href="', $scripturl, '?action=help">', $txt['help'], '</a> | <a href="', $scripturl, '?action=help;sa=rules">', $txt['terms_and_rules'], '</a></li>
			</ul>
						', theme_copyright(), '
					</div>';

				// Show the load time?
				if ($context['show_load_time'])
					echo '
					<p>', $txt['page_created'], $context['load_time'], $txt['seconds_with'], $context['load_queries'], $txt['queries'], '</p>';

				echo '
				</div>
			</div>
		</footer>';
}

function template_html_below()
{
	global $context, $settings, $scripturl, $txt, $modSettings;

	template_javascript(true);

	echo '
	<script type="text/javascript">
		$(document).ready(function(){
			$("input[type=text], input[type=password], input[type=file], input[type=number], input[type=search], textarea").attr("class", "form-control");
			$("input[type=button]").attr("class", "btn btn-default btn-sm");
			$(".button_submit").attr("class", "btn btn-primary btn-sm");
			$("input").removeAttr("size");
			$(".table_grid").attr("class", "table table-striped");
			$(".new_posts").addClass("label label-warning");
			$("img[alt=\'', $txt['new'], '\'], img.new_posts").replaceWith("<span class=\'label label-warning\'>', $txt['new'], '</span>");
			$("p.alert").remove();
			$("#profile_success").removeAttr("id").removeClass("windowbg").addClass("alert alert-success");
			$("#profile_error").removeAttr("id").removeClass("windowbg").addClass("alert alert-danger");
			$(".errorbox").attr("class", "alert alert-danger");
		});function loadJSFile(f){var e=document.createElement("script");e.src=f;document.body.appendChild(e);}
	</script>
</body>
</html>';
}

/**
 * Show a linktree. This is that thing that shows "My Community | General Category | General Discussion"..
 * @param bool $force_show = false
 */
function theme_linktree($force_show = false)
{
	global $context, $settings, $shown_linktree, $scripturl, $txt;

	// If linktree is empty, just return - also allow an override.
	if (empty($context['linktree']) || (!empty($context['dont_default_linktree']) && !$force_show))
		return;

	echo '
		<ul class="breadcrumb">';

	// Each tree item has a URL and name. Some may have extra_before and extra_after.
	foreach ($context['linktree'] as $tree)
	{
		echo '
						<li>';

		// Show something before the link?
		if (isset($tree['extra_before']))
			echo $tree['extra_before'], ' ';

		// Show the link, including a URL if it should have one.
		echo isset($tree['url']) ? '
							<a href="' . $tree['url'] . '">' . $tree['name'] . '</a>' : $tree['name'];

		// Show something after the link...?
		if (isset($tree['extra_after']))
			echo ' ', $tree['extra_after'];

		echo '
						</li>';
	}

	echo '
					</ul>';

	$shown_linktree = true;
}

/**
 * Show the menu up top. Something like [home] [help] [profile] [logout]...
 */
function template_menu()
{
	global $context, $txt, $scripturl;

	foreach ($context['menu_buttons'] as $act => $button)
	{
		echo '
				<li id="button_', $act, '" class="', !empty($button['sub_buttons']) ? 'dropdown ' : '', '', $button['active_button'] ? 'active ' : '', '">
					<a ', !empty($button['sub_buttons']) ? 'class="dropdown-toggle" ' : '', 'href="', !empty($button['sub_buttons']) ? '#' : $button['href'], '"', isset($button['target']) ? ' target="' . $button['target'] . '"' : '', !empty($button['sub_buttons']) ? ' data-toggle="dropdown"' : '', '>
						', $button['title'], '
						', !empty($button['sub_buttons']) ? '<span class="caret"></span>' : '' , '
					</a>';
		if (!empty($button['sub_buttons']))
		{
			echo '
					<ul class="dropdown-menu" role="menu">';

			foreach ($button['sub_buttons'] as $childbutton)
			{
				echo '
						<li>
							<a href="', $childbutton['href'], '"', isset($childbutton['target']) ? ' target="' . $childbutton['target'] . '"' : '', '>
								', $childbutton['title'] , '
							</a>
						</li>';
			}
				echo '
					</ul>';
		}
		echo '
				</li>';
	}
		echo '
			</ul>';

		echo'
			<ul class="nav navbar-nav navbar-right">';

	if (!$context['user']['is_logged'])
		echo'
			<form class="navbar-form" action="', $scripturl, '?action=login2" method="post" accept-charset="', $context['character_set'], '">
				<input type="text" name="user" class="form-control" placeholder="', $txt['user'] ,'" />
				<input type="password" name="passwrd" class="form-control" placeholder="', $txt['password'] ,'" />
				<button type="submit" class="btn btn-success">', $txt['login'] ,'</button>
				<input name="cookielength" type="hidden" value="-1" />
				<input type="hidden" name="', $context['session_var'], '" value="', $context['session_id'], '">
				<input type="hidden" name="', $context['login_token_var'], '" value="', $context['login_token'], '">
			</form>';

	if ($context['allow_search'])
	{
		echo '
				<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" id="search_menu_top"><span aria-hidden="true" class="glyphicon glyphicon-search"></span>
					<b class="caret"></b></a>
					<ul class="dropdown-menu">
			<li>
			<form id="search_form" action="', $scripturl, '?action=search2" method="post" accept-charset="', $context['character_set'], '">
				<input type="search" name="search" value="" class="input_text" />&nbsp;';

			$selected = !empty($context['current_topic']) ? 'current_topic' : (!empty($context['current_board']) ? 'current_board' : 'all');

			echo '
				<select name="search_selection">
					<option value="all"', ($selected == 'all' ? ' selected="selected"' : ''), '>', $txt['search_entireforum'], ' </option>';

			// Can't limit it to a specific topic if we are not in one
			if (!empty($context['current_topic']))
				echo '
					<option value="topic"', ($selected == 'current_topic' ? ' selected="selected"' : ''), '>', $txt['search_thistopic'], '</option>';

		// Can't limit it to a specific board if we are not in one
		if (!empty($context['current_board']))
			echo '
					<option value="board"', ($selected == 'current_board' ? ' selected="selected"' : ''), '>', $txt['search_thisbrd'], '</option>';
			echo '
					<option value="members"', ($selected == 'members' ? ' selected="selected"' : ''), '>', $txt['search_members'], ' </option>
				</select>';

		// Search within current topic?
		if (!empty($context['current_topic']))
			echo '
				<input type="hidden" name="', (!empty($modSettings['search_dropdown']) ? 'sd_topic' : 'topic'), '" value="', $context['current_topic'], '" />';
		// If we're on a certain board, limit it to this board ;).
		elseif (!empty($context['current_board']))
			echo '
				<input type="hidden" name="', (!empty($modSettings['search_dropdown']) ? 'sd_brd[' : 'brd['), $context['current_board'], ']"', ' value="', $context['current_board'], '" />';

		echo '
				<input type="submit" name="search2" value="', $txt['search'], '" class="btn btn-success btn-sm" />
				<input type="hidden" name="advanced" value="0" />
					<div id="search_menu"></div>
			</form>
						</li>
					</ul>
				</li>';
	}

	if ($context['user']['is_logged'])
		echo'
				<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" id="alerts_menu_top">
				<span aria-hidden="true" class="fa fa-bell"></span>
					<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li class="panel panel-default keepopen">
							<div class="panel-heading">
								<div class="row">
									<div class="col-md-6">
										<a class="btn btn-primary btn-sm" href="', $scripturl, '?action=profile;area=notification;sa=markread;', $context['session_var'], '=', $context['session_id'], '" onclick="return markAlertsRead(this)">', $txt['mark_alerts_read'], '</a>
									</div>
									<div class="col-md-6">
										<a class="btn btn-default btn-sm pull-right" href="', $scripturl, '?action=profile;area=notification;sa=alerts">', $txt['alert_settings'], '</a>
									</div>
								</div>
							</div>
							<div class="panel-body clear" id="alerts_menu"></div>
							<div class="panel-footer centertext">
								<a class="btn btn-success btn-sm" href="', $scripturl, '?action=profile;area=showalerts">', $txt['all_alerts'], '</a>
							</div>
						</li>
					</ul>
				</li>
				<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
				<span aria-hidden="true" class="glyphicon glyphicon-user"></span>
					<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li class="panel panel-default keepopen">
							<div class="panel-body">
								<div class="row">
									<div class="col-md-5">
									<img src="', $context['user']['avatar']['href'], '" class="avatar img-responsive" alt="*" />
										<p class="text-center small">
											<a href="#">Change Photo</a></p>
									</div>
									<div class="col-md-7">
										<span>', $context['user']['name'], '</span>
										<p class="text-muted">', $context['user']['group'], '</p>
										<a href="', $scripturl, '?action=unread">', $txt['unread_topics_visit'], '</a> | <a href="', $scripturl, '?action=unreadreplies">', $txt['unread_replies'], '</a>
										<div class="divider"></div>
										<a class="btn btn-success btn-sm active" href="', $scripturl, '?action=profile">', $txt['profile'], '</a>
									</div>
								</div>
							</div>
							<div class="panel-footer">
								<div class="navbar-foot3er-content">
									<div class="row">
										<div class="col-md-6">
											<a class="btn btn-default btn-sm" href="', $scripturl, '?action=profile;area=forumprofile">', $txt['forumprofile'], '</a>
										</div>
										<div class="col-md-6">
											<a href="', $context['menu_buttons']['logout']['href'], '" class="btn btn-primary btn-sm pull-right">', $context['menu_buttons']['logout']['title'], '</a>
										</div>
									</div>
								</div>
							</div>
						</li>
					</ul>
				</li>';
	return;

	echo '
				<div id="main_menu">
					<ul class="nav">';

	// Note: Menu markup has been cleaned up to remove unnecessary spans and classes.
	foreach ($context['menu_buttons'] as $act => $button)
	{
		echo '
						<li id="button_', $act, '"', !empty($button['sub_buttons']) ? ' class="subsections"' :'', '>
							<a', $button['active_button'] ? ' class="active"' : '', ' href="', $button['href'], '"', isset($button['target']) ? ' target="' . $button['target'] . '"' : '', '>
								', $button['title'], '
							</a>';
		if (!empty($button['sub_buttons']))
		{
			echo '
							<ul>';

			foreach ($button['sub_buttons'] as $childbutton)
			{
				echo '
								<li', !empty($childbutton['sub_buttons']) ? ' class="subsections"' :'', '>
									<a href="', $childbutton['href'], '"' , isset($childbutton['target']) ? ' target="' . $childbutton['target'] . '"' : '', '>
										', $childbutton['title'], '
									</a>';
				// 3rd level menus :)
				if (!empty($childbutton['sub_buttons']))
				{
					echo '
									<ul>';

					foreach ($childbutton['sub_buttons'] as $grandchildbutton)
						echo '
										<li>
											<a href="', $grandchildbutton['href'], '"' , isset($grandchildbutton['target']) ? ' target="' . $grandchildbutton['target'] . '"' : '', '>
												', $grandchildbutton['title'], '
											</a>
										</li>';

					echo '
									</ul>';
				}

				echo '
								</li>';
			}
				echo '
							</ul>';
		}
		echo '
						</li>';
	}

	echo '
					</ul>
				</div>';
}

/**
 * Generate a strip of buttons.
 * @param array $button_strip
 * @param string $direction = ''
 * @param array $strip_options = array()
 */
function template_button_strip($button_strip, $direction = '', $strip_options = array())
{
	global $context, $txt;

	if (!is_array($strip_options))
		$strip_options = array();

	$class = 'button_strip btn-group';

	if (!empty($direction))
		$class .= ' float' . $direction;

	if (!empty($strip_options['class']))
		$class .= ' ' . $strip_options['class'];

	// List the buttons in reverse order for RTL languages.
	if ($context['right_to_left'])
		$button_strip = array_reverse($button_strip, true);

	// Create the buttons...
	$buttons = array();
	foreach ($button_strip as $key => $value)
	{
		// @todo this check here doesn't make much sense now (from 2.1 on), it should be moved to where the button array is generated
		// Kept for backward compatibility
		if (!isset($value['test']) || !empty($context[$value['test']]))
		{
			$this_class = 'button_strip_' . $key;
			if (!empty($value['sub_buttons']))
				$this_class .= ' dropdown';

			if (!empty($value['active']))
				$this_class .= ' btn btn-success';
			else
				$this_class .= ' btn btn-primary';

			$button = '';
			if (!empty($value['sub_buttons']))
				$button .= '
				<span>';

			$button .= '
				<a' . (!empty($this_class) ? ' class="' . $this_class . '"' : '') . (!empty($value['sub_buttons']) ? ' class="dropdown-toggle"' : '') . ' href="'		 . (!empty($value['sub_buttons']) ? '#' : $value['url']) . '"' . (isset($value['custom']) ? ' ' . $value['custom'] : '') . (!empty($value['sub_buttons']) ? ' data-toggle="dropdown"' : '') . '>' . $txt[$value['text']] . (!empty($value['sub_buttons']) ? ' <span class="caret"></span>' : '') . '</a>';

			if (!empty($value['sub_buttons']))
			{
				$button .= '
					<ul class="dropdown-menu" role="menu">';

				foreach ($value['sub_buttons'] as $element)
				{
					if (isset($element['test']) && empty($context[$element['test']]))
						continue;

					$button .= '
							<li>
								<a href="' . $element['url'] . '"><strong class="largetext lower_padding">' . $txt[$element['text']] . '</strong>';

					if (isset($txt[$element['text'] . '_desc']))
						$button .= '<p>' . $txt[$element['text'] . '_desc'] . '</p>';
					$button .= '</a>
							</li>';
				}
				$button .= '
						</ul>';
				$button .= '</span>';
			}

			$buttons[] = $button;
		}
	}

	// No buttons? No button strip either.
	if (empty($buttons))
		return;

	if (!empty($strip_options['return']))
		return $buttons;

	echo '
		<div class="', $class, '"', !empty($strip_options['id']) ? ' id="' . $strip_options['id'] . '"': '', '>
			', implode('', $buttons), '
		</div>';
}

function template_maint_warning_above()
{
	global $txt, $context, $scripturl;

	echo '
	<div class="errorbox" id="errors">
		<dl>
			<dt>
				<strong id="error_serious">', $txt['forum_in_maintainence'], '</strong>
			</dt>
			<dd class="error" id="error_list">
				', sprintf($txt['maintenance_page'], $scripturl . '?action=admin;area=serversettings;' . $context['session_var'] . '=' . $context['session_id']), '
			</dd>
		</dl>
	</div>';
}

function template_maint_warning_below() {}

function template_list_boards($boards)
{
	global $context, $txt, $scripturl;

			/* Each board in each category's boards has:
			new (is it new?), id, name, description, moderators (see below), link_moderators (just a list.),
			children (see below.), link_children (easier to use.), children_new (are they new?),
			topics (# of), posts (# of), link, href, and last_post. (see below.) */
			foreach ($boards as $board)
			{
				echo '
				<div id="board_', $board['id'], '" class="row">
					<div class="col-xs-2 col-md-1">
						<a href="', ($board['is_redirect'] || $context['user']['is_guest'] ? $board['href'] : $scripturl . '?action=unread;board=' . $board['id'] . '.0;children'), '">
							<span class="feature-icon board-', $board['board_class'], '"', !empty($board['board_tooltip']) ? ' title="' . $board['board_tooltip'] . '"' : '', '></span>
						</a>
					</div>
					<div class="col-xs-10 col-sm-7 col-md-6">
						<h5><a class="subject" href="', $board['href'], '" id="b', $board['id'], '">', $board['name'], '</a>';

				// Has it outstanding posts for approval?
				if ($board['can_approve_posts'] && ($board['unapproved_posts'] || $board['unapproved_topics']))
					echo '
						<a href="', $scripturl, '?action=moderate;area=postmod;sa=', ($board['unapproved_topics'] > 0 ? 'topics' : 'posts'), ';brd=', $board['id'], ';', $context['session_var'], '=', $context['session_id'], '" title="', sprintf($txt['unapproved_posts'], $board['unapproved_topics'], $board['unapproved_posts']), '" class="moderation_link">(!)</a>';

				echo '

						</h5><p>', $board['description'] , '</p>';

				// Show the "Moderators: ". Each has name, href, link, and id. (but we're gonna use link_moderators.)
				if (!empty($board['link_moderators']))
					echo '
						<p class="hidden-xs hidden-sm">', count($board['link_moderators']) == 1 ? $txt['moderator'] : $txt['moderators'], ': ', implode(', ', $board['link_moderators']), '</p>';

				// Show some basic information about the number of posts, etc.
				echo '
					</div>
					<div class="col-sm-3 col-md-2 hidden-xs">
						<p>', comma_format($board['posts']), ' ', $board['is_redirect'] ? $txt['redirects'] : $txt['posts'], '
						', $board['is_redirect'] ? '' : '<br> ' . comma_format($board['topics']) . ' ' . $txt['board_topics'], '
						</p>
					</div>
					<div class="col-md-3 hidden-xs hidden-sm">';

				if (!empty($board['last_post']['id']))
					echo '
						<p>', $board['last_post']['last_post_message'], '</p>';
				echo '
					</div>';
				// Show the "Child Boards: ". (there's a link_children but we're going to bold the new ones...)
				if (!empty($board['children']))
				{
					// Sort the links into an array with new boards bold so it can be imploded.
					$children = array();
					/* Each child in each board's children has:
							id, name, description, new (is it new?), topics (#), posts (#), href, link, and last_post. */
					foreach ($board['children'] as $child)
					{
						if (!$child['is_redirect'])
							$child['link'] = '<a href="' . $child['href'] . '" ' . ($child['new'] ? 'class="board_new_posts" ' : '') . 'title="' . ($child['new'] ? $txt['new_posts'] : $txt['old_posts']) . ' (' . $txt['board_topics'] . ': ' . comma_format($child['topics']) . ', ' . $txt['posts'] . ': ' . comma_format($child['posts']) . ')">' . $child['name'] . ($child['new'] ? '</a> <a href="' . $scripturl . '?action=unread;board=' . $child['id'] . '" title="' . $txt['new_posts'] . ' (' . $txt['board_topics'] . ': ' . comma_format($child['topics']) . ', ' . $txt['posts'] . ': ' . comma_format($child['posts']) . ')"><span class="new_posts">' . $txt['new'] . '</span>' : '') . '</a>';
						else
							$child['link'] = '<a href="' . $child['href'] . '" title="' . comma_format($child['posts']) . ' ' . $txt['redirects'] . ' - ' . $child['short_description'] . '">' . $child['name'] . '</a>';

						// Has it posts awaiting approval?
						if ($child['can_approve_posts'] && ($child['unapproved_posts'] || $child['unapproved_topics']))
							$child['link'] .= ' <a href="' . $scripturl . '?action=moderate;area=postmod;sa=' . ($child['unapproved_topics'] > 0 ? 'topics' : 'posts') . ';brd=' . $child['id'] . ';' . $context['session_var'] . '=' . $context['session_id'] . '" title="' . sprintf($txt['unapproved_posts'], $child['unapproved_topics'], $child['unapproved_posts']) . '" class="moderation_link">(!)</a>';

						$children[] = $child['new'] ? '<strong>' . $child['link'] . '</strong>' : $child['link'];
					}

				echo '
					<div class="col-xs-12 hidden-xs hidden-sm">
						<strong>', $txt['sub_boards'], '</strong><div class="row"><div class="col-md-6 col-lg-4">', implode('</div><div class="col-md-6 col-lg-4">', $children), '</div></div>
					</div>';
				}

				echo '
					</div>';
			}

}

function template_list_boards2($boards)
{
	global $context, $txt, $scripturl;

				echo 'This is a list of all the mods available on this site.
				</div>';

			echo '
				<div class="row list-group">';

			foreach ($boards as $board)
			{
				echo '
					<a class="col-md-6 col-lg-4 list-group-item" href="', $board['href'], '" id="b', $board['id'], '">
						', $board['name'];

				// Has it outstanding posts for approval?
				if ($board['can_approve_posts'] && ($board['unapproved_posts'] || $board['unapproved_topics']))
					echo '
						<a href="', $scripturl, '?action=moderate;area=postmod;sa=', ($board['unapproved_topics'] > 0 ? 'topics' : 'posts'), ';brd=', $board['id'], ';', $context['session_var'], '=', $context['session_id'], '" title="', sprintf($txt['unapproved_posts'], $board['unapproved_topics'], $board['unapproved_posts']), '" class="moderation_link">(!)</a>';

				echo '
					</a>';
			}

}

?>