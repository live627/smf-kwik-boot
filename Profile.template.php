<?php
/**
 * Simple Machines Forum (SMF)
 *
 * @package SMF
 * @author Simple Machines
 * @copyright 2014 Simple Machines and individual contributors
 * @license http://www.simplemachines.org/about/smf/license.php BSD
 *
 * @version 2.1 Alpha 1
 */

function template_Profile_init()
{
	global $settings;

	require_once($settings['default_theme_dir'] . '/Profile.template.php');
}

// Template for showing off the spiffy popup of the menu
function template_profile_popup_override()
{
	global $context, $scripturl, $txt;

	// Set the age...
	if (empty($context['member']['birth_date']))
	{
		$context['member'] += array(
			'age' => $txt['not_applicable'],
			'today_is_birthday' => false
		);
	}
	else
	{
		list ($birth_year, $birth_month, $birth_day) = sscanf($context['member']['birth_date'], '%d-%d-%d');
		$datearray = getdate(forum_time());
		$context['member'] += array(
			'age' => $birth_year <= 4 ? $txt['not_applicable'] : $datearray['year'] - $birth_year - (($datearray['mon'] > $birth_month || ($datearray['mon'] == $birth_month && $datearray['mday'] >= $birth_day)) ? 0 : 1),
			'today_is_birthday' => $datearray['mon'] == $birth_month && $datearray['mday'] == $birth_day
		);
	}

	// Unlike almost every other template, this is designed to be included into the HTML directly via $().load()

	echo '
		<div class="container">
			<div class="row">
				<div class="row">
					<div class="col-sm-6 col-md-4">
						', $context['member']['avatar']['image'],'
					</div>
					<div class="col-sm-6 col-md-8">
						<h4>', $context['member']['name'], '</h4>';

			if (!isset($context['disabled_fields']['location']) && !empty($context['member']['location']))
				echo '
						<small><i class="glyphicon glyphicon-map-marker"></i> ', $context['member']['location'], '</small>';

				echo '
						<p>';

			// Only show the email address fully if it's not hidden - and we reveal the email.
			if ($context['member']['show_email'])
				echo '
							<i class="glyphicon glyphicon-envelope"></i> <a href="', $scripturl, '?action=emailuser;sa=email;uid=', $context['member']['id'], '">', $context['member']['email'], '</a>';

				// ... Or if the one looking at the profile is an admin they can see it anyway.
				elseif (!$context['member']['show_email'] && allowedTo('moderate_forum'))
					echo '
							<i class="glyphicon glyphicon-envelope"></i> <em><a href="', $scripturl, '?action=emailuser;sa=email;uid=', $context['member']['id'], '">', $context['member']['email'], '</a></em>';
				else
					echo '
							<i class="glyphicon glyphicon-envelope"></i> <em>', $txt['hidden'], '</em>';

			echo '
							<br />';

			if ($context['member']['website']['url'] != '' && !isset($context['disabled_fields']['website']))
				echo '
						<i class="glyphicon glyphicon-globe"></i> <a href="', $context['member']['website']['url'], '" target="_blank">', $context['member']['website']['title'], '</a></dd>';

			echo '
							<br />
							<i class="glyphicon glyphicon-gift"></i> ', $context['member']['age'] . ($context['member']['today_is_birthday'] ? ' &nbsp; <img src="' . $settings['images_url'] . '/bdaycake.gif" width="40" alt="" />' : ''), '</p>
						<!-- Split button -->
						<div class="btn-group">
							<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">', $txt['more'], '
								<span class="caret"></span><span class="sr-only">', $txt['more'], '</span>
							</button>
							<ul class="dropdown-menu" role="menu">';

	$menu_context = &$context[$context['profile_menu_name']];
	$menu = [];
	foreach ($context['profile_items'] as $item)
	{
		$menu[$item['area']][] = $item;
	}
	foreach ($menu as $m)
	{
			echo '
								<li class="nav-header">', $menu_context['sections'][$item['menu']]['title'], '</li>
								<li class="divider"></li>';
		foreach ($m as $item)
		{
			$area = &$menu_context['sections'][$item['menu']]['areas'][$item['area']];
			$item_url = (isset($item['url']) ? $item['url'] : (isset($area['url']) ? $area['url'] : $menu_context['base_url'] . ';area=' . $item['area'])) . $menu_context['extra_parameters'];
			echo '
								<li><a href="', $item_url, '">', $area['icon'], '', !empty($item['title']) ? $item['title'] : $area['label'], '</a></li>';
		}
	}

	echo '
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>';
}

function template_alerts_popup_override()
{
	global $context, $txt, $scripturl;

	if (empty($context['unread_alerts']))
		echo '
			<div class="no_unread">', $txt['alerts_no_unread'], '</div>';
	else
		foreach ($context['unread_alerts'] as $id_alert => $details)
			echo '
			<div class="media">
				<div class="media-left">', !empty($details['sender']) ? $details['sender']['avatar']['image'] : '', '</div>
				<div class="media-body">
					', !empty($details['icon']) ? $details['icon'] : '', $details['text'], '<br>
					<em class="text-muted smalltext">', $details['time'], '</em>
				</div>
			</div>';
}

// This template displays users details without any option to edit them.
function template_summary_override()
{
	global $context, $settings, $scripturl, $modSettings, $txt;

	// Display the basic information about the user
	echo '
<div id="profileview" class="flexcontainer">
		<div>
			<div class="cat_bar">
				<h3 class="catbg">
					<img src="', $settings['images_url'], '/icons/profile_sm.gif" alt="" />&nbsp;', $txt['summary'], ' - ', $context['member']['name'], '
				</h3>
			</div>
			<div class="roundframe">
				<div class="centertext floatleft" style="width: 35%;">
					', $context['member']['avatar']['image'];

			if (!empty($context['member']['blurb']))
				echo '
						<div style="padding-top: 1em;" class="smalltext"><em>', $context['member']['blurb'], '</em></div>';

			echo '
					</div>
					<dl class="settings">
						<dt><strong>', $txt['name'], ': </strong></dt>
						<dd>', $context['member']['name'], '</dd>';

			if (!empty($modSettings['titlesEnable']) && !empty($context['member']['title']))
				echo '
						<dt><strong>', $txt['custom_title'], ': </strong></dt>
						<dd>', $context['member']['title'], '</dd>';

			if (!isset($context['disabled_fields']['posts']))
				echo '
						<dt><strong>', $txt['profile_posts'], ': </strong></dt>
						<dd>', $context['member']['posts'], ' (', $context['member']['posts_per_day'], ' ', $txt['posts_per_day'], ')</dd>';
			echo '
						<dt><strong>', $txt['position'], ': </strong></dt>
						<dd>', (!empty($context['member']['group']) ? $context['member']['group'] : $context['member']['post_group']), '</dd>
						<dt><strong>', $txt['status'], ': </strong></dt>
						<dd>', $context['member']['online']['text'], '</dd>
					</dl>
					<dl class="settings clear">';

			if (!isset($context['disabled_fields']['gender']) && !empty($context['member']['gender']['name']))
				echo '
						<dt><strong>', $txt['gender'], ': </strong></dt>
						<dd>', $context['member']['gender']['image'] ,' ', $context['member']['gender']['name'], '</dd>';

				echo '
						<dt><strong>', $txt['age'], ':</strong></dt>
						<dd>', $context['member']['age'] . ($context['member']['today_is_birthday'] ? ' &nbsp; <img src="' . $settings['images_url'] . '/bdaycake.gif" width="40" alt="" />' : ''), '</dd>';

			if (!isset($context['disabled_fields']['location']) && !empty($context['member']['location']))
				echo '
						<dt><strong>', $txt['location'], ':</strong></dt>
						<dd>', $context['member']['location'], '</dd>';

				echo '
						<dt><strong>', $txt['date_registered'], ': </strong></dt>
						<dd>', $context['member']['registered'], '</dd>
						<dt><strong>', $txt['lastLoggedIn'], ': </strong></dt>
						<dd>', $context['member']['last_login'], '</dd>
					</dl>';

			// Any custom fields for standard placement?
			if (!empty($context['custom_fields']))
			{
				$shown = false;
				foreach ($context['custom_fields'] as $field)
				{
					if ($field['placement'] != 0 || empty($field['output_html']))
						continue;

					if (empty($shown))
					{
						echo '
					<dl class="settings">';
						$shown = true;
					}

					echo '
						<dt><strong>', $field['name'], ':</strong></dt>
						<dd>', $field['output_html'], '</dd>';
				}

				if (!empty($shown))
					echo '
					</dl>';
			}

			// Are there any custom profile fields for the summary?
			if (!empty($context['custom_fields']))
			{
				$shown = false;
				foreach ($context['custom_fields'] as $field)
				{
					if ($field['placement'] == 1 || empty($field['output_html']))
					{
						if (empty($shown))
						{
							echo '
					<dl class="settings">';
							$shown = true;
						}

						echo '
						<dt><strong>', $field['name'], ':</strong></dt>
						<dd>', $field['output_html'], '</dd>';
					}
				}

				if (!empty($shown))
					echo '
					</dl>';
			}

			echo '
			</div>
		</div>
		<div>
			<div class="cat_bar">
				<h3 class="catbg">
					Additional Information
				</h3>
			</div>
			<div class="roundframe">';

	// Are there any custom profile fields for the summary?
	if (!empty($context['custom_fields']))
	{
		$shown = false;
		foreach ($context['custom_fields'] as $field)
		{
			if ($field['placement'] != 2 || empty($field['output_html']))
				continue;

			if (empty($shown))
			{
				echo '
					<dl class="settings">';
				$shown = true;
			}

			echo '
						<dt><strong>', $field['name'], ':</strong></dt>
						<dd>', $field['output_html'], '</dd>';
		}

		if (!empty($shown))
			echo '
					</dl>';
	}

	// Show the users signature.
	if ($context['signature_enabled'] && !empty($context['member']['signature']))
		echo '
				<div class="signature">
					<h5>', $txt['signature'], ':</h5>
					', $context['member']['signature'], '
				</div>
				<hr size="1" width="100%" class="hrcolor" />';

	echo '
				<dl class="settings">';

	// Can they view/issue a warning?
	if ($context['can_view_warning'] && $context['member']['warning'])
	{
		echo '
				<dt>', $txt['profile_warning_level'], ': </dt>
				<dd>
					<a href="', $scripturl, '?action=profile;u=', $context['id_member'], ';area=', $context['can_issue_warning'] ? 'issuewarning' : 'viewwarning', '">', $context['member']['warning'], '%</a>';

		// Can we provide information on what this means?
		if (!empty($context['warning_status']))
			echo '
					<span class="smalltext">(', $context['warning_status'], ')</span>';

		echo '
				</dd>';
	}

	// Is this member requiring activation and/or banned?
	if (!empty($context['activate_message']) || !empty($context['member']['bans']))
	{

		// If the person looking at the summary has permission, and the account isn't activated, give the viewer the ability to do it themselves.
		if (!empty($context['activate_message']))
			echo '
				<dt class="clear"><span class="alert">', $context['activate_message'], '</span>&nbsp;(<a href="' . $scripturl . '?action=profile;save;area=activateaccount;u=' . $context['id_member'] . ';' . $context['session_var'] . '=' . $context['session_id'] . '"', ($context['activate_type'] == 4 ? ' onclick="return confirm(\'' . $txt['profileConfirm'] . '\');"' : ''), '>', $context['activate_link_text'], '</a>)</dt>';

		// If the current member is banned, show a message and possibly a link to the ban.
		if (!empty($context['member']['bans']))
		{
			echo'
				<dt class="clear"><span class="alert">', $txt['user_is_banned'], '</span>&nbsp;[<a href="#" onclick="document.getElementById(\'ban_info\').style.display = document.getElementById(\'ban_info\').style.display == \'none\' ? \'\' : \'none\';return false;">' . $txt['view_ban'] . '</a>]</dt>
				<dt class="clear" id="ban_info" style="display: none;">
					<strong>', $txt['user_banned_by_following'], ':</strong>';

			foreach ($context['member']['bans'] as $ban)
				echo '
					<br /><span class="smalltext">', $ban['explanation'], '</span>';

			echo '
				</dt>';
		}
	}

	// If the person looking is allowed, they can check the members IP address and hostname.
	if ($context['can_see_ip'])
	{
		if (!empty($context['member']['ip']))
			echo '
						<dt><strong>', $txt['ip'], ': </strong></dt>
						<dd><a href="', $scripturl, '?action=profile;area=tracking;sa=ip;searchip=', $context['member']['ip'], ';u=', $context['member']['id'], '">', $context['member']['ip'], '</a></dd>';

		if (empty($modSettings['disableHostnameLookup']) && !empty($context['member']['ip']))
			echo '
						<dt><strong>', $txt['hostname'], ': </strong></dt>
						<dd>', $context['member']['hostname'], '</dd>';
	}

	if (!empty($modSettings['userLanguage']) && !empty($context['member']['language']))
		echo '
						<dt><strong>', $txt['language'], ':</strong></dt>
						<dd>', $context['member']['language'], '</dd>';

	echo '
						<dt><strong>', $txt['local_time'], ':</strong></dt>
						<dd>', $context['member']['local_time'], '</dd>
					</dl>
			</div>
			<div class="cat_bar">
				<h3 class="catbg">
					Contact Info
				</h3>
			</div>
			<div class="roundframe">';

			// Can they add this member as a buddy?
			if (!empty($context['can_have_buddy']) && !$context['user']['is_owner'] && !$context['member']['is_buddy'])
				echo '
						<img src="', $settings['images_url'] ,'/icons/online.gif" alt="" valign="middle" /> <a href="', $scripturl, '?action=buddies;sa=add;u=', $context['member']['id'], ';sesc=', $context['session_id'], '" onclick="javascript:return confirm(\'', $txt['buddy_explanation']  ,'\')">[', $txt['buddy_add'], ']</a><br />';

			if (!$context['user']['is_owner'] && $context['can_send_pm'])
				echo '
						<img src="', $settings['images_url'] ,'/icons/pm_read.gif" alt="" valign="middle" /> <a href="', $scripturl, '?action=pm;sa=send;u=', $context['member']['id'], '">', $txt['send_member_pm'], '.</a><br />';

			if ((!empty($context['can_have_buddy']) && !$context['user']['is_owner']) || (!$context['user']['is_owner'] && $context['can_send_pm']))
				echo '
						<hr width="100%" />';

				echo '
					<dl class="settings">
						<dt><img src="', $settings['images_url'] ,'/email_sm.gif" alt="', $txt['email'], '" /></dt>
						<dd>';

			// Only show the email address fully if it's not hidden - and we reveal the email.
			if ($context['member']['show_email'] == 'yes')
				echo '
							<a href="', $scripturl, '?action=emailuser;sa=email;uid=', $context['member']['id'], '">', $context['member']['email'], '</a>';

				// ... Or if the one looking at the profile is an admin they can see it anyway.
				elseif ($context['member']['show_email'] == 'yes_permission_override')
					echo '
							<em><a href="', $scripturl, '?action=emailuser;sa=email;uid=', $context['member']['id'], '">', $context['member']['email'], '</a></em>';
				else
					echo '
							<em>', $txt['hidden'], '</em>';

			// Some more information.
			echo '
						</dd>';

			if ($context['member']['website']['url'] != '' && !isset($context['disabled_fields']['website']))
				echo '
						<dt><img src="', $settings['images_url'] ,'/www.gif" alt="', $txt['website'], '" /></dt>
						<dd><a href="', $context['member']['website']['url'], '" target="_blank">', $context['member']['website']['title'], '</a></dd>';

			echo '
					</dl>
			</div>
		</div>
</div>';
}

function template_statPanel_override()
{
	global $context, $txt;

	// First, show a few text statistics such as post/topic count.
	echo '
	<div id="profileview" class="roundframe">
		<div id="generalstats">
			<dl class="stats">
				<dt>', $txt['statPanel_total_time_online'], ':</dt>
				<dd>', $context['time_logged_in'], '</dd>
				<dt>', $txt['statPanel_total_posts'], ':</dt>
				<dd>', $context['num_posts'], ' ', $txt['statPanel_posts'], '</dd>
				<dt>', $txt['statPanel_total_topics'], ':</dt>
				<dd>', $context['num_topics'], ' ', $txt['statPanel_topics'], '</dd>
				<dt>', $txt['statPanel_users_polls'], ':</dt>
				<dd>', $context['num_polls'], ' ', $txt['statPanel_polls'], '</dd>
				<dt>', $txt['statPanel_users_votes'], ':</dt>
				<dd>', $context['num_votes'], ' ', $txt['statPanel_votes'], '</dd>
			</dl>
		</div>';

	// This next section draws a graph showing what times of day they post the most.
	echo '
		<div id="activitytime" class="flow_hidden">
			<div class="title_bar">
				<h3 class="titlebg">
					<span class="generic_icons history"></span> ', $txt['statPanel_activityTime'], '
				</h3>
			</div>';

	// If they haven't post at all, don't draw the graph.
	if (empty($context['posts_by_time']))
		echo '
			<span class="centertext">', $txt['statPanel_noPosts'], '</span>';
	// Otherwise do!
	else
	{
		echo '
			<ul class="activity_stats flow_hidden">';

		// The labels.
		foreach ($context['posts_by_time'] as $time_of_day)
		{
			echo '
				<li', $time_of_day['is_last'] ? ' class="last"' : '', '>
					<div class="bar" style="padding-top: ', ((int) (100 - $time_of_day['relative_percent'])), 'px;" title="', sprintf($txt['statPanel_activityTime_posts'], $time_of_day['posts'], $time_of_day['posts_percent']), '">
						<div style="height: ', (int) $time_of_day['relative_percent'], 'px;">
							<span>', sprintf($txt['statPanel_activityTime_posts'], $time_of_day['posts'], $time_of_day['posts_percent']), '</span>
						</div>
					</div>
					<span class="stats_hour">', $time_of_day['hour_format'], '</span>
				</li>';
		}

		echo '

			</ul>';
	}

	echo '
			<span class="clear">
		</div>';

	// Two columns with the most popular boards by posts and activity (activity = users posts / total posts).
	echo '
		<div class="flow_hidden">
			<div class="half_content">
				<div class="title_bar">
					<h3 class="titlebg">
						<span class="generic_icons replies"></span> ', $txt['statPanel_topBoards'], '
					</h3>
				</div>';

	if (empty($context['popular_boards']))
		echo '
				<span class="centertext">', $txt['statPanel_noPosts'], '</span>';

	else
	{
		echo '
				<dl class="stats">';

		// Draw a bar for every board.
		foreach ($context['popular_boards'] as $board)
		{
			echo '
					<dt>', $board['link'], '</dt>
					<dd>
						<div class="profile_pie" style="background-position: -', ((int) ($board['posts_percent'] / 5) * 20), 'px 0;" title="', sprintf($txt['statPanel_topBoards_memberposts'], $board['posts'], $board['total_posts_member'], $board['posts_percent']), '">
							', sprintf($txt['statPanel_topBoards_memberposts'], $board['posts'], $board['total_posts_member'], $board['posts_percent']), '
						</div>
						', empty($context['hide_num_posts']) ? $board['posts'] : '', '
					</dd>';
		}

		echo '
				</dl>';
	}
	echo '
			</div>';
	echo '
			<div class="half_content">
				<div class="title_bar">
					<h3 class="titlebg">
						<span class="generic_icons replies"></span> ', $txt['statPanel_topBoardsActivity'], '
					</h3>
				</div>';

	if (empty($context['board_activity']))
		echo '
				<span>', $txt['statPanel_noPosts'], '</span>';
	else
	{
		echo '
				<dl class="stats">';

		// Draw a bar for every board.
		foreach ($context['board_activity'] as $activity)
		{
			echo '
					<dt>', $activity['link'], '</dt>
					<dd>
						<div class="profile_pie" style="background-position: -', ((int) ($activity['percent'] / 5) * 20), 'px 0;" title="', sprintf($txt['statPanel_topBoards_posts'], $activity['posts'], $activity['total_posts'], $activity['posts_percent']), '">
							', sprintf($txt['statPanel_topBoards_posts'], $activity['posts'], $activity['total_posts'], $activity['posts_percent']), '
						</div>
						', $activity['percent'], '%
					</dd>';
		}

		echo '
				</dl>';
	}
	echo '
			</div>
		</div>';

	echo '
	</div>';
}

// Template for choosing group membership.
function template_groupMembership_override()
{
	global $context, $settings, $scripturl, $modSettings, $txt;

	// The main containing header.
	echo '
		<form action="', $scripturl, '?action=profile;area=groupmembership;save" method="post" accept-charset="', $context['character_set'], '" name="creator" id="creator" class="row">';

	// Requesting membership to a group?
	if (!empty($context['group_request']))
	{
		echo '
			<div class="groupmembership">
				<div class="roundframe">
					', $txt['request_group_membership_desc'], ':
					<textarea name="reason" rows="4" style="' . (isBrowser('is_ie8') ? 'width: 635px; max-width: 99%; min-width: 99%' : 'width: 99%') . ';"></textarea>
					<div class="footer">
						<input type="hidden" name="gid" value="', $context['group_request']['id'], '" />
						<input type="submit" name="req" value="', $txt['submit_request'], '" class="btn btn-primary" />
					</div>
				</div>
			</div>';
	}
	else
	{
		if (!empty($context['update_message']))
			echo '
			<div class="infobox">
				', $context['update_message'], '.
			</div>';

		echo '
			<div class="list-group col-xs-12 col-md-6">
				<div class="list-group-item">
					<h4 class="list-group-item-heading">
						', $txt['current_membergroups'], '
					</h4>
				</div>';

		foreach ($context['groups']['member'] as $group)
		{
			echo '
				<div class="row list-group-item', $group['is_primary'] ? ' list-group-item-info' : '', '">
					<div class="col-xs-8">
						<h4 class="list-group-item-heading">', (empty($group['color']) ? $group['name'] : '<span style="color: ' . $group['color'] . '">' . $group['name'] . '</span>'), '</h4>', (!empty($group['desc']) ? '
						<p class="list-group-item-text">' . $group['desc'] . '</p>' : ''), '</label>
					</div>
					<div class="col-xs-4">';

				if ($context['can_edit_primary'])
					echo '
						<button type="submit" name="primary" id="primary_', $group['id'], '" value="', $group['id'], '"', $group['is_primary'] ? ' checked' : '', ' onclick="highlightSelected(\'primdiv_' . $group['id'] . '\');" ', $group['can_be_primary'] ? '' : 'disabled="disabled"', ' class="btn btn-primary">', $txt['make_primary'], '</button>';

				// Can they leave their group?
				if ($group['can_leave'])
					echo '
						<a href="' . $scripturl . '?action=profile;save;u=' . $context['id_member'] . ';area=groupmembership;' . $context['session_var'] . '=' . $context['session_id'] . ';gid=' . $group['id'] . ';', $context[$context['token_check'] . '_token_var'], '=', $context[$context['token_check'] . '_token'], '" role="button" class="btn btn-default">' . $txt['leave_group'] . '</a>';
				echo '
					</div>
				</div>';
		}

		echo '
			</div>';

		// Any groups they can join?
		if (!empty($context['groups']['available']))
		{
			echo '
			<div class="list-group col-xs-12 col-md-6">
				<div class="list-group-item">
					<h4 class="list-group-item-heading">
						', $txt['available_groups'], '
					</h4>
				</div>';

			$alternate = true;
			foreach ($context['groups']['available'] as $group)
			{
				echo '
				<div class="row list-group-item', $group['type'] == 2 && $group['pending'] ? ' list-group-item-warning' : '', '">

					<div class="col-xs-8">
						<h4 class="list-group-item-heading">', (empty($group['color']) ? $group['name'] : '<span style="color: ' . $group['color'] . '">' . $group['name'] . '</span>'), '</h4>', (!empty($group['desc']) ? '
						<p class="list-group-item-text">' . $group['desc'] . '</p>' : ''), '</label>
					</div>
					<div class="col-xs-4">';

				if ($group['type'] == 3)
					echo '
							<a href="', $scripturl, '?action=profile;save;u=', $context['id_member'], ';area=groupmembership;', $context['session_var'], '=', $context['session_id'], ';gid=', $group['id'], ';', $context[$context['token_check'] . '_token_var'], '=', $context[$context['token_check'] . '_token'], '" role="button" class="btn btn-default">', $txt['join_group'], '</a>';
				elseif ($group['type'] == 2 && $group['pending'])
					echo '
							', $txt['approval_pending'];
				elseif ($group['type'] == 2)
					echo '
							<a href="', $scripturl, '?action=profile;u=', $context['id_member'], ';area=groupmembership;request=', $group['id'], '" role="button" data-title="', $txt['request_group_membership'], '" class="btn btn-default" onclick="return reqWin(this.href);">', $txt['request_group'], '</a>';

				echo '
					</div>
				</div>';
			}

			echo '
			</div>';
		}

		// Javascript for the selector stuff.
		echo '
		<script><!-- // --><![CDATA[
		function highlightSelected(box)
		{
		}
	// ]]></script>';
	}

	if (!empty($context['token_check']))
		echo '
				<input type="hidden" name="', $context[$context['token_check'] . '_token_var'], '" value="', $context[$context['token_check'] . '_token'], '" />';

	echo '
				<input type="hidden" name="', $context['session_var'], '" value="', $context['session_id'], '" />
				<input type="hidden" name="u" value="', $context['id_member'], '" />
			</form>';
}
function template_editBuddies_override()
{
	global $context, $scripturl, $txt;

	if (!empty($context['saved_successful']))
		echo '
					<div class="infobox">', $context['user']['is_owner'] ? $txt['profile_updated_own'] : sprintf($txt['profile_updated_else'], $context['member']['name']), '</div>';
	elseif (!empty($context['saved_failed']))
		echo '
					<div class="errorbox">', $context['saved_failed'], '</div>';

	echo '
	<div id="edit_buddies">
		<div class="cat_bar">
			<h3 class="catbg">
				<span class="generic_icons people icon"></span> ', $txt['buddy_remove'], '
			</h3>
		</div>
		<div class="row list-group">';

	if (empty($context['buddies']))
		echo '
			<div class="list-group-item list-group-item-info">', $txt['no_buddies'], '</div>';
	else
		foreach ($context['buddies'] as $buddy)
			echo '
			<a class="col-md-6 col-lg-4 list-group-item" href="', $scripturl, '?action=profile;area=lists;sa=buddies;u=', $context['id_member'], ';remove=', $buddy['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $buddy['name'], '</a>';

	echo '
		</div>
	</div>';

	// Add a new buddy?
	echo '
	<form action="', $scripturl, '?action=profile;u=', $context['id_member'], ';area=lists;sa=buddies" method="post" accept-charset="', $context['character_set'], '">
		<div class="cat_bar">
			<h3 class="catbg">', $txt['buddy_add'], '</h3>
		</div>
		<dl class="settings windowbg">
			<dt>
				<label for="new_buddy"><strong>', $txt['who_member'], ':</strong></label>
			</dt>
			<dd>
				<input type="text" name="new_buddy" id="new_buddy" size="30" class="input_text">
				<input type="submit" value="', $txt['buddy_add_button'], '" class="button_submit floatnone">
			</dd>
		</dl>';

	if (!empty($context['token_check']))
		echo '
			<input type="hidden" name="', $context[$context['token_check'] . '_token_var'], '" value="', $context[$context['token_check'] . '_token'], '">';

	echo '
		<input type="hidden" name="', $context['session_var'], '" value="', $context['session_id'], '">
	</form>
	<script><!-- // --><![CDATA[
		var oAddBuddySuggest = new smc_AutoSuggest({
			sSelf: \'oAddBuddySuggest\',
			sSessionId: smf_session_id,
			sSessionVar: smf_session_var,
			sSuggestId: \'new_buddy\',
			sControlId: \'new_buddy\',
			sSearchType: \'member\',
			sTextDeleteItem: \'', $txt['autosuggest_delete_item'], '\',
			bItemList: false
		});
	// ]]></script>';
}

// Template for showing the ignore list of the current user.
function template_editIgnoreList_override()
{
	global $context, $scripturl, $txt;

	if (!empty($context['saved_successful']))
		echo '
					<div class="infobox">', $context['user']['is_owner'] ? $txt['profile_updated_own'] : sprintf($txt['profile_updated_else'], $context['member']['name']), '</div>';
	elseif (!empty($context['saved_failed']))
		echo '
					<div class="errorbox">', $context['saved_failed'], '</div>';

	echo '
	<div id="edit_buddies">
		<div class="cat_bar">
			<h3 class="catbg profile_hd">
				', $txt['ignore_remove'], '
			</h3>
		</div>
		<div class="row list-group">';

	// If they don't have anyone on their ignore list, don't list it!
	if (empty($context['ignore_list']))
		echo '
			<div class="list-group-item list-group-item-info">', $txt['no_ignore'], '</div>';

	foreach ($context['ignore_list'] as $member)
		echo '
			<a class="col-md-6 col-lg-4 list-group-item" href="', $scripturl, '?action=profile;u=', $context['id_member'], ';area=lists;sa=ignore;remove=', $member['id'], ';', $context['session_var'], '=', $context['session_id'], '">', $member['name'], '</a>';

	echo '
		</div>
	</div>';

	// Add to the ignore list?
	echo '
	<form action="', $scripturl, '?action=profile;u=', $context['id_member'], ';area=lists;sa=ignore" method="post" accept-charset="', $context['character_set'], '">
		<div class="cat_bar">
			<h3 class="catbg">', $txt['ignore_add'], '</h3>
		</div>
		<dl class="settings windowbg">
			<dt>
				<label for="new_buddy"><strong>', $txt['who_member'], ':</strong></label>
			</dt>
			<dd>
				<input type="text" name="new_ignore" id="new_ignore" size="25" class="input_text">
			</dd>
		</dl>';

	if (!empty($context['token_check']))
		echo '
		<input type="hidden" name="', $context[$context['token_check'] . '_token_var'], '" value="', $context[$context['token_check'] . '_token'], '">';

	echo '
		<input type="hidden" name="', $context['session_var'], '" value="', $context['session_id'], '">
		<input type="submit" value="', $txt['ignore_add_button'], '" class="button_submit">
	</form>
	<script><!-- // --><![CDATA[
		var oAddIgnoreSuggest = new smc_AutoSuggest({
			sSelf: \'oAddIgnoreSuggest\',
			sSessionId: \'', $context['session_id'], '\',
			sSessionVar: \'', $context['session_var'], '\',
			sSuggestId: \'new_ignore\',
			sControlId: \'new_ignore\',
			sSearchType: \'member\',
			sTextDeleteItem: \'', $txt['autosuggest_delete_item'], '\',
			bItemList: false
		});
	// ]]></script>';
}

?>