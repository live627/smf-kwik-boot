$(document).ready(function() {
	var cssId = 'myCss';  // you could encode the css path itself to generate id..
	if ($('.fa').length)
	{
		var head  = document.getElementsByTagName('head')[0];
		var link  = document.createElement('link');
		link.id   = cssId;
		link.rel  = 'stylesheet';
		link.type = 'text/css';
		link.href = smf_theme_url + '/css/font-awesome.css';
		link.media = 'all';
		head.appendChild(link);
	}

	var imgDefer = document.getElementsByTagName('img');
	for (var i=0; i<imgDefer.length; i++) {
		if(imgDefer[i].getAttribute('data-src')) {
			imgDefer[i].setAttribute('src',imgDefer[i].getAttribute('data-src'));
		}
	}
	//$('img').attr('src', $(this).data('src'));

	$('select').selectpicker();
	$('input[maxlength][type=text]').maxlength();
	prettyPrint();
	// menu drop downs
	//$('ul.dropmenu, ul.quickbuttons').superfish({delay : 600, speed: 200, sensitivity : 8, interval : 50, timeout : 1});

	// tooltips
	$('.preview').SMFtooltip();

	// find all nested linked images and turn off the border
	$('a.bbc_link img.bbc_img').parent().css('border', '0');

	$profile_links = $( "[href*='action=profile;u=']:not(.btn)" ).data('remote', smf_prepareScriptUrl(smf_scripturl) + 'action=profile;area=popup').data('title', $(this).attr('title'));

	// Setting the DOM element's onclick to null removes
	// the inline click handler
	$("[onclick*=req]").each(function ()
	{
		$(this)[0].onclick = null;
	});
	$('[onclick*=req]').click(function (e)
	{
		$('#ajaxModal').remove();
		e.preventDefault();
		var $this = $(this)
		  , t = $this.data('title') || help_popup_heading_text
		  , $modal = $('<div class="modal fade" id="ajaxModal"><div class="modal-dialog modal-lg">    <div class="modal-content">      <div class="modal-header">        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>        <h4 class="modal-title" id="myModalLabel">' + t + '</h4>      </div>      <div class="modal-body"><i class="loading"></i></div>      <div class="modal-footer">        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button></div>    </div><!-- /.modal-content -->  </div><!-- /.modal-dialog --></div>');
		$('body').append($modal);
		$modal.modal();
		$('.modal-body', $modal).load($remote, function ( response, status, xhr )
		{
			if ( status == "error" )
				$(this).html( "Sorry but there was an error: " + xhr.status + " " + xhr.statusText );

			if ($(this).find('form'))
			{
				$('.modal-footer', $modal).hide();
				$(this).find('.footer').prepend($('button', $modal)).addClass('modal-footer');
			}

			$(this).find('a[href$="self.close();"]').hide().prev('br').hide();
		});

		// Return false so the click won't follow the link ;).
		return false;
	});
	$($profile_links).click(function (e)
	{
		$('#ajaxModal').remove();
		e.preventDefault();
		var $this = $(this)
		  , $remote = $this.data('remote') || $this.attr('href')
		  , t = $this.data('title') || help_popup_heading_text
		  , $modal = $('<div class="modal fade" id="ajaxModal"><div class="modal-dialog modal-lg">    <div class="modal-content">      <div class="modal-header">        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>        <h4 class="modal-title" id="myModalLabel">' + t + '</h4>      </div>      <div class="modal-body"><i class="loading"></i></div>      <div class="modal-footer">        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button></div>    </div><!-- /.modal-content -->  </div><!-- /.modal-dialog --></div>');
		$('body').append($modal);
		$modal.modal();
		$('.modal-body', $modal).load($remote, function ( response, status, xhr )
		{
			if ( status == "error" )
				$(this).html( "Sorry but there was an error: " + xhr.status + " " + xhr.statusText );

			if ($(this).find('form'))
			{
				$('.modal-footer', $modal).hide();
				$(this).find('.footer').prepend($('button', $modal)).addClass('modal-footer');
			}

			$(this).find('a[href$="self.close();"]').hide().prev('br').hide();
		});

		// Return false so the click won't follow the link ;).
		return false;
	});
});

// Toggles the element height and width styles of an image.
function smc_toggleImageDimensions()
{
	var images = $('img.bbc_img');

	$.each(images, function(key, img)
	{
		if ($(img).hasClass('resized'))
		{
			$(img).css({cursor: 'pointer'});
			$(img).on('click', function()
			{
				var size = $(this)[0].style.width == 'auto' ? '' : 'auto';
				$(this).css({width: size, height: size});
			});
		}
	});
}

// Add a load event for the function above.
addLoadEvent(smc_toggleImageDimensions);

function smf_addButton(stripId, image, options)
{
	$('#' + stripId + ' ul').append(
		'<li' + ('sId' in options ? ' id="' + options.sId + '"' : '') + '>' +
			'<a href="' + options.sUrl + '"' + ('sCustom' in options ? options.sCustom : '') + '>' +
				'<span class="last"' + ('sId' in options ? ' id="' + options.sId + '_text"' : '') + '>' + options.sText + '</span>' +
			'</a>' +
		'</li>'
	);
}

/* ========================================================================
* Bootstrap: bootstrap-dropdown-multilevel.js v1.0.0
* http://getbootstrap.com/javascript/#dropdowns
* ========================================================================
* Copyright 2011-2014 Twitter, Inc.
* Copyright 2014 George Donev.
* Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
* ======================================================================== */


+function ($) {
	'use strict';

	// DROPDOWN CLASS DEFINITION
	// =========================

	var backdrop = '.dropdown-backdrop'
	var toggle = '[data-toggle=dropdown]'
	var Dropdown = function (element) {
		$(element).on('click.bs.dropdown', this.toggle)
	}

	Dropdown.prototype.toggle = function (e) {
		var $this = $(this)

		if ($this.is('.disabled, :disabled')) return

		var $parent = getParent($this)
		var isActive = $parent.hasClass('open')

		clearMenus($(this))

		if (!isActive) {
			if ('ontouchstart' in document.documentElement && !$parent.closest('.navbar-nav').length) {
				// if mobile we use a backdrop because click events don't delegate
				$('<div class="dropdown-backdrop"/>').insertAfter($(this)).on('click', clearMenus)
			}

			var relatedTarget = { relatedTarget: this }
			$parent.trigger(e = $.Event('show.bs.dropdown', relatedTarget))

			if (e.isDefaultPrevented()) return

			$parent
				.toggleClass('open')
				.trigger('shown.bs.dropdown', relatedTarget)

			$this.focus()
		}

		return false
	}

	Dropdown.prototype.keydown = function (e) {
		if (!/(38|40|27)/.test(e.keyCode)) return

		var $this = $(this)

		e.preventDefault()
		e.stopPropagation()

		if ($this.is('.disabled, :disabled')) return

		var $parent = getParent($this)
		var isActive = $parent.hasClass('open')

		if (!isActive || (isActive && e.keyCode == 27)) {
			if (e.which == 27) $parent.find(toggle).focus()
			return $this.click()
		}

		var desc = ' li:not(.divider):visible a'
		var $items = $parent.find('[role=menu]' + desc + ', [role=listbox]' + desc)

		if (!$items.length) return

		var index = $items.index($items.filter(':focus'))

		if (e.keyCode == 38 && index > 0) index-- // up
		if (e.keyCode == 40 && index < $items.length - 1) index++ // down
		if (!~index) index = 0

		$items.eq(index).focus()
	}

	function clearMenus(e) {
		$(backdrop).remove()
		$(toggle).each(function () {
			var $parent = getParent($(this));
			var $childMenus = $parent.find('.dropdown');
			if ($childMenus.length) {
		 var $DoNotCloseThisOne = false;
		 $childMenus.each(function(){
				 if ($($(this).find(':first-child')[0]).is(e)) $DoNotCloseThisOne = true;
					})
					if ($DoNotCloseThisOne) return
			}
			var relatedTarget = { relatedTarget: this }
			if (!$parent.hasClass('open')) return
    			$parent.trigger(e = $.Event('hide.bs.dropdown', relatedTarget))
			if (e.isDefaultPrevented()) return
			$parent.removeClass('open').trigger('hidden.bs.dropdown', relatedTarget)
		})
	}

	function getParent($this) {
		var selector = $this.attr('data-target')

		if (!selector) {
			selector = $this.attr('href')
			selector = selector && /#[A-Za-z]/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, '') //strip for ie7
		}

		var $parent = selector && $(selector)

		return $parent && $parent.length ? $parent : $this.parent()
	}


	// DROPDOWN PLUGIN DEFINITION
	// ==========================

	//var old = $.fn.dropdown

	$.fn.dropdown = function (option) {
		return this.each(function () {
			var $this = $(this)
			var data = $this.data('bs.dropdown')

			if (!data) $this.data('bs.dropdown', (data = new Dropdown(this)))
			if (typeof option == 'string') data[option].call($this)
		})
	}

	$.fn.dropdown.Constructor = Dropdown


	// DROPDOWN NO CONFLICT
	// ====================

	$.fn.dropdown.noConflict = function () {
		//$.fn.dropdown = old
		return this
	}



	$(document)
	 .off('click.bs.dropdown.data-api', "**")
		.off('keydown.bs.dropdown.data-api', "**")

	// APPLY TO STANDARD DROPDOWN ELEMENTS
	// ===================================
	$(document)
		.on('click.bs.dropdown.data-api', clearMenus)
		.on('click.bs.dropdown.data-api', '.dropdown form', function (e) { e.stopPropagation() })
		.on('click.bs.dropdown.data-api', '.dropdown .panel-body', function (e) { e.stopPropagation() })
		.on('click.bs.dropdown.data-api', toggle, Dropdown.prototype.toggle)
		.on('keydown.bs.dropdown.data-api', toggle + ', [role=menu], [role=listbox]', Dropdown.prototype.keydown)

}(jQuery);

var aBoardsAndCategories = [];

// This function will retrieve the contents needed for the jump to boxes.
function grabJumpToContent(elem)
{
	var oXMLDoc = getXMLDocument(smf_prepareScriptUrl(smf_scripturl) + 'action=xmlhttp;sa=jumpto;xml');

	ajax_indicator(true);

	if (oXMLDoc.responseXML)
	{
		var items = oXMLDoc.responseXML.getElementsByTagName('smf')[0].getElementsByTagName('item');
		for (var i = 0, n = items.length; i < n; i++)
		{
			aBoardsAndCategories[aBoardsAndCategories.length] = {
				id: parseInt(items[i].getAttribute('id')),
				isCategory: items[i].getAttribute('type') == 'category',
				name: items[i].firstChild.nodeValue.removeEntities(),
				is_current: false,
				childLevel: parseInt(items[i].getAttribute('childlevel'))
			}
		}
	}

	ajax_indicator(false);
}

// *** JumpTo class.
function JumpTo(opt)
{
	$('#' + opt.sContainerId)
		.html('<select title=" ' + opt.sCurBoardName + '"></select>')
		.css({ visibility: 'visible' })
		.find('select').selectpicker('refresh').on('click.bs.select', function ()
		{
			var sList = '', $val, $this = $(this);

			grabJumpToContent(this);

					$.each(aBoardsAndCategories, function ()
					{
						// Just for the record, we don't NEED to close the optgroup at the end
						// of the list, even if it doesn't feel right. Saves us a few bytes...
						if (!this.isCategory) // Show the board option, with special treatment for the current one.
							sList += '<option value="' + this.id + '"'
									+ (this.id == opt.iCurBoardId ? ' selected disabled>=> ' + this.name + ' &lt;=' :
										'>' + new Array(+this.childLevel + 1).join('&nbsp;&nbsp;&nbsp;&nbsp;') + this.name)
									+ '</option>';
						else // Category?
							sList += '<optgroup label="' + this.name + '">';
					});

					// Add the remaining items after the currently selected item.
					$this.off('click.bs.select').append(sList).selectpicker('refresh').change(function () {
						location.assign(smf_prepareScriptUrl(smf_scripturl) + 'board=' + this.options[this.selectedIndex].value);
					});
		});
}

// Simple function to add a hidden element for form submission
function addHiddenElement(formName, oValue, oName)
{
	var parent = document.forms[formName];
	var hidden = document.createElement("input");
	hidden.type = "hidden";
	hidden.value = oValue;
	hidden.name = oName;

	parent.appendChild(hidden);
}