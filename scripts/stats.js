for (e in pbt)
{
	var ctx = $('#' + e + '_canvas').get(0).getContext('2d');
	var data = {
		labels: pbt[e]['labels'],
		datasets: [
			{
				fillColor: 'rgba(220,220,220,0.5)',
				strokeColor: 'rgba(220,220,220,1)',
				data: pbt[e]['data']
			}
		]
	}
	new Chart(ctx).HorizontalBar(data, {
			inGraphDataShow: true,
			annotateDisplay: true,
			annotateClassName: "tooltip",
		}
	);
}
$('#reportrange').daterangepicker(
	{
		ranges: {
			'Today': [moment(), moment()],
			'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
			'Last 7 Days': [moment().subtract('days', 6), moment()],
			'Last 30 Days': [moment().subtract('days', 29), moment()],
			'This Month': [moment().startOf('month'), moment().endOf('month')],
			'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
		},
		startDate: moment().subtract('days', 29),
		endDate: moment()
	},
	function(start, end) {
		$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
	}
);

function drawHistory ()
{
	datasets = [];

	if (m['labels'].length == 1)
		m = d;

	for (e in colors)
		if (m[e])
			datasets[datasets.length] = {
				fillColor: e != 'hits' && e != 'new_posts' ? 'transparent' : 'rgba(' + colors[e] + ',0.5)',
				strokeColor: 'rgb(' + colors[e] + ')',
				pointColor: 'rgb(' + colors[e] + ')',
				pointStrokeColor: '#fff',
				data: m[e],
				title: txt[e]
			}

	var ctx = $('#history_canvas').get(0).getContext('2d');
	var data = {
		labels: m['labels'],
		datasets: datasets
	}
	new Chart(ctx).Line(data, {
			savePng: true,
			savePngFunction: "click",
			inGraphDataShow: true,
			annotateDisplay: true,
			annotateLabel: "<%=(v1 == '' ? '' : v1) + (v1!='' && v2 !='' ? ' - ' : '')+(v2 == '' ? '' : v2)+(v1!='' || v2 !='' ? ': ' : '') + v3%>",
			annotateClassName: "tooltip",
			legend: true
		}
	);
}

var user_menus = new smc_PopupMenu();
user_menus.add('stats', smf_prepareScriptUrl(smf_scripturl) + 'action=statpop');

var
	sessionData = {
		'um': {}
	},
	sessionName = 'live627:theme1.uHistSel',
	um = {},
	restoredSession = localStorage.getItem(sessionName);

if (restoredSession)
{
	sessionData = JSON.parse(restoredSession);
	um = sessionData.um ;
}

updateCanvas();

function getUserInput ()
{
	var
		$chksm = $('#stats_menu input[type=checkbox]');

	$chksm.each(function ()
	{
		var $this = $(this);
		if ($this.is(':checked') || sessionData.um[$this.val()])
			um[$this.val()] = 1;

		if (sessionData.um[$this.val()])
			$this.prop("checked", true);
	});

	$chksm.change(function ()
	{
		var
			$this = $(this),
			chk = $this.is(':checked');

		if ($this.is(':checked'))
			um[$this.val()] = 1;
		else
			delete um[$this.val()];

		sessionData = {
			'um': um,
		};
		localStorage.setItem(sessionName, JSON.stringify({}));

		updateCanvas();
	});
}
function updateCanvas ()
{
	var uri = '';

	$.each(um, function(i, y1)
	{
		var mi = i.split('-');
		uri += 'h[' + mi[0] + ']=' + mi[1] + '&';
	});

	$.getScript(smf_prepareScriptUrl(smf_scripturl) + 'action=statsel;' + uri, function (data)
	{
		drawHistory();
	});
}