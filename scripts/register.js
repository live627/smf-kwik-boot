function smfRegister2(formID, passwordDifficultyLevel, regTextStrings)
{
	var
		verificationFields = [],
		textStrings = regTextStrings ? regTextStrings : [],
		passwordLevel = passwordDifficultyLevel ? passwordDifficultyLevel : 0;

	$('#' + formID).find('input[id*="autov"]').each(function ()
	{
		var curType = 0, eventHandler = false, id = this.id;

		if (id.indexOf('username') != -1)
		{
			curType = 'username';
			eventHandler = refreshUsername;
		}
		else if (id.indexOf('email') != -1)
		{
			curType = 'email';
			eventHandler = refreshEmail;
		}
		else if (id.indexOf('pwmain') != -1)
		{
			curType = 'pwmain';
			eventHandler = refreshMainPassword;
		}
		else if (id.indexOf('pwverify') != -1)
		{
			curType = 'pwverify';
			eventHandler = refreshVerifyPassword;
		}
		if (curType)
		{
			var $inputHandle = $('#' + id);
			if ($inputHandle.length)
			{
				verificationFields[curType] = [
					id, $inputHandle[0]
				];

				if (eventHandler)
				{
					$inputHandle.keyup(eventHandler).blur(autoCheckUsername);
					eventHandler();
				}
			}
		}

		if (curType == 'username')
			$('#' + id + '_link').click(checkUsername);
	});

	function verifyPass()
	{
		if (!verificationFields.pwmain)
			return false;

		var curPass = verificationFields.pwmain[1].value, stringIndex = '';

		if ((curPass.length < 8 && passwordLevel >= 1) || curPass.length < 4)
			stringIndex = 'password_short';

		if (passwordLevel >= 1)
		{
			if (verificationFields.username && verificationFields.username[1].value && curPass.indexOf(verificationFields.username[1].value) != -1)
				stringIndex = 'password_reserved';

			if ((passwordLevel > 1) && ((curPass == curPass.toLowerCase()) || (!curPass.match(/(\D\d|\d\D)/))))
				stringIndex = 'password_numbercase';
		}

		return stringIndex == '';
	}

	function refreshMainPassword()
	{
		if (!verificationFields.pwmain)
			return false;

		var curPass = verificationFields.pwmain[1].value, stringIndex = '';

		if ((curPass.length < 8 && passwordLevel >= 1) || curPass.length < 4)
			stringIndex = 'password_short';

		if (passwordLevel >= 1)
		{
			if (verificationFields.username && verificationFields.username[1].value && curPass.indexOf(verificationFields.username[1].value) != -1)
				stringIndex = 'password_reserved';

			if ((passwordLevel > 1) && ((curPass == curPass.toLowerCase()) || (!curPass.match(/(\D\d|\d\D)/))))
				stringIndex = 'password_numbercase';
		}

		var
			isValid = verifyPass(),
			status = isValid ? 'success' : 'error',
			glyphicon = isValid ? 'ok' : 'remove',
			label = isValid ? 'success' : 'danger';

		if (stringIndex == '')
			stringIndex = 'password_valid';

		if (isEmptyText(curPass))
		{
			stringIndex = '';
			status = 'warning';
			glyphicon = 'warning-sign';
			label = 'warning';
		}

		$('#' + verificationFields.pwmain[0] + '_div').attr('class', 'form-group has-feedback has-' + status).find('span').attr('class', 'glyphicon glyphicon-' + glyphicon + ' form-control-feedback').next().attr('class', 'label label-' + label).text(textStrings[stringIndex] ? textStrings[stringIndex] : '');
		refreshVerifyPassword();
		$('#' + verificationFields.pwmain[0]).complexify({minimumChars: passwordLevel >= 1 ? 8 : 4, strengthScaleFactor: 0.7}, function (valid, complexity)
		{
			var
				calculated = (complexity/100)*268 - 134,
				prop = 'rotate('+(calculated)+'deg)';

			$('.progress div').css({'width': complexity + '%'}).attr('aria-valuenow', complexity).parent().next().text(Math.round(complexity) + '%');
		});

		return isValid;
	}

	function refreshVerifyPassword()
	{
		if (!verificationFields.pwverify)
			return false;

		var
			isVerified = verifyPass(),
			isValid = verificationFields.pwmain[1].value == verificationFields.pwverify[1].value && isVerified,
			alt = textStrings[isValid ? 'password_valid' : 'password_no_match'],
			status = isValid ? 'success' : 'error',
			glyphicon = isValid ? 'ok' : 'remove',
			label = isValid ? 'success' : 'danger';

		if (isEmptyText(verificationFields.pwmain[1].value))
		{
			alt = '';
			status = 'warning';
			glyphicon = 'warning-sign';
			label = 'warning';
		}

		$('#' + verificationFields.pwverify[0] + '_div').attr('class', 'form-group has-feedback has-' + status).find('span').attr('class', 'glyphicon glyphicon-' + glyphicon + ' form-control-feedback').next().attr('class', 'label label-' + label).text(alt);

		return true;
	}

	function refreshUsername()
	{
		if (!verificationFields.username)
			return false;

		$('#' + verificationFields.username[0] + '_div').attr('class', 'form-group has-warning has-feedback').find('span').attr('class', 'glyphicon glyphicon-warning-sign form-control-feedback').next().attr('class', 'label label-warning').text('');

		refreshMainPassword();

		return true;
	}

	function autoCheckUsername()
	{
		checkUsername(true);
	}

	function checkUsername(is_auto)
	{
		if (!verificationFields.username)
			return false;

		var curUsername = verificationFields.username[1].value;
		if (!curUsername)
			return false;

		if (!is_auto)
			ajax_indicator(true);

		getXMLDocument(smf_prepareScriptUrl(smf_scripturl) + 'action=signup;sa=usernamecheck;xml;username=' + encodeURIComponent(curUsername), checkUsernameCallback);

		return true;
	}

	function checkUsernameCallback(XMLDoc)
	{
		var
			isValid = $('username', XMLDoc).attr('valid') == 1,
			status = isValid ? 'success' : 'error',
			glyphicon = isValid ? 'ok' : 'remove',
			label = isValid ? 'success' : 'danger',
			alt = textStrings[isValid == 1 ? 'username_valid' : 'username_invalid'];

		$('#' + verificationFields.username[0] + '_div').attr('class', 'form-group has-feedback has-' + status).find('span').attr('class', 'glyphicon glyphicon-' + glyphicon + ' form-control-feedback').next().attr('class', 'label label-' + label).text(alt);

		ajax_indicator(false);
	}

	function refreshEmail()
	{
		var
			isValid = /^\S+@\S+\.\S+$/.test($('#' + verificationFields.email[0]).val()),
			status = isValid ? 'success' : 'error',
			glyphicon = isValid ? 'ok' : 'remove',
			label = isValid ? 'success' : 'danger',
			alt = textStrings[isValid == 1 ? 'email_valid' : 'email_invalid'];

		if (isEmptyText(verificationFields.email[1].value))
		{
			alt = '';
			status = 'warning';
			glyphicon = 'warning-sign';
			label = 'warning';
		}

		$('#' + verificationFields.email[0] + '_div').attr('class', 'form-group has-feedback has-' + status).find('span').attr('class', 'glyphicon glyphicon-' + glyphicon + ' form-control-feedback').next().attr('class', 'label label-' + label).text(alt);
	}
}