<?php

function changeButtons(&$buttons)
{
	global $txt, $context, $scripturl, $board, $topic;

	// Add Forum to the linktree.
	if (!empty($board) || !empty($topic) || $context['current_action'] == 'forum')
		array_splice($context['linktree'], 1, 0, array(array(
				'name' => $txt['forum'],
				'url' => $scripturl . '?action=forum',
			))
		);

	$new = array(
		'title' => (!empty($txt['forum']) ? $txt['forum'] : 'Forum'),
		'href' => $scripturl . '?action=forum',
		'show' => true,
		'active_button' => false,
	);

	$new_buttons = array();
	foreach ($buttons as $area => $info)
	{
		$new_buttons[$area] = $info;
		if ($area == 'home')
			$new_buttons['forum'] = $new;
	}

	$buttons = $new_buttons;

	$new = array(
		'title' => $txt['contact'],
		'href' => $scripturl . '?action=contact',
		'show' => true,
	);

	$new_button = array(
		'title' => $txt['members_title'],
		'href' => $scripturl . '?action=members',
		'show' => $context['allow_memberlist'],
		'asub_buttons' => array(
			'mlist_view' => array(
				'title' => $txt['mlist_menu_view'],
				'href' => $scripturl . '?action=mlist',
				'show' => true,
			),
			'mlist_search' => array(
				'title' => $txt['mlist_search'],
				'href' => $scripturl . '?action=mlist;sa=search',
				'show' => true,
				'is_last' => true,
			),
		),
	);

	$new_buttons = array();
	foreach ($buttons as $area => $info)
	{
		$new_buttons[$area] = $info;
		if ($area == 'search')
			$new_buttons['contact'] = $new;
		if ($area == 'mlist')
			$new_buttons['members'] = $new_button;
	}

	$buttons = $new_buttons;

	unset($buttons['mlist']);

	$buttons['admin']['action_hook'] = true;

	if (AJAX)
	{
		$context['template_layers'] = [];
		$db_show_debug = false;
	}

}

function fixBBC(&$codes)
{
	global $modSettings, $user_info;

	// Make sure the admin had not disabled img tags as well
	if (!empty($modSettings['disabledBBC']))
	{
		foreach (explode(',', $modSettings['disabledBBC']) as $tag)
			if ($tag === 'img')
				return;
	}

	// Find the quote bbc tags and update how they render their HTML
	foreach ($codes as &$code)
	{
		if ($code['tag'] === 'quote')
		{
			$code['before'] = str_replace(array('<div class="quoteheader">', '</div><blockquote>'), array('<blockquote><cite>', '</cite>'), $code['before']);
			$code['after'] = '</blockquote>';
		}
	}
}

function fixErrors()
{
	global $context;

	$context['error'] = true;
}

function fixSubTemplates($current_action)
{
	global $context;

	$new_sub_template = empty($context['sub_template']) ? 'main_override' : $context['sub_template'] . '_override';

	if (is_callable('template_' . $new_sub_template))
		$context['sub_template'] = $new_sub_template;
}

function fixMarkup($buffer)
{
	global $settings;

	$js = '';
	preg_match_all('#<script(.*?)<\/script>#is', $buffer, $matches);
	foreach ($matches[0] as $value) {
		$js .= '
	'.$value;
		$buffer = str_replace($value, '', $buffer);
	}
	$buffer = str_replace('</body>', $js.'</body>' , $buffer);
	$buffer = str_replace('<title>', '
    <script>function loadFont(a,b,c,d){function e(){if(!window.FontFace)return!1;var a=new FontFace("t",\'url("data:application/font-woff2,") format("woff2")\',{}),b=a.load();try{b.then(null,function(){})}catch(c){}return"loading"===a.status}var f=navigator.userAgent,g=!window.addEventListener||f.match(/(Android (2|3|4.0|4.1|4.2|4.3))|(Opera (Mini|Mobi))/)&&!f.match(/Chrome/);if(!g){var h={};try{h=localStorage||{}}catch(i){}var j="x-font-"+a,k=j+"url",l=j+"css",m=h[k],n=h[l],o=document.createElement("style");if(o.rel="stylesheet",document.head.appendChild(o),!n||m!==b&&m!==c){var p=c&&e()?c:b,q=new XMLHttpRequest;q.open("GET",p),q.onload=function(){q.status>=200&&q.status<400&&(h[k]=p,h[l]=q.responseText,d||(o.textContent=q.responseText))},q.send()}else o.textContent=n}}</script>
    <!-- This script must be placed in the HEAD above all external stylesheet declarations (link[rel=stylesheet]) -->
    <script>loadFont(\'sourceSansPro\', \'' . $settings['theme_url'] . '/css/fonts.woff.css\', \'' . $settings['theme_url'] . '/css/fonts.woff2.css\')</script><title>' , $buffer);

	preg_match_all('/<img[^>]+\>/i', $buffer, $images, PREG_SET_ORDER);
	foreach ($images as $image) {
		$img = $image[0];
		$new_img = str_replace('src=', 'src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src=', $img);
		$buffer = str_replace($img, $new_img, $buffer);
	}

	return $buffer;
}

function default_action()
{
	global $modSettings, $user_info;

	// Don't continue if they're a guest and guest access is off.
	if (!empty($modSettings['allow_guestAccess']) && $user_info['is_guest'])
		return;

	// XML mode? Nothing more is required of us...
	if (isset($_REQUEST['xml']))
		return;

	global $context, $txt;

	// A mobile device doesn't require a portal...
	if (WIRELESS)
		redirectexit('action=forum');

	$context['page_title'] = $context['forum_name'] . ' - ' . $txt['home'];
	loadTemplate('Portal', 'home');
	loadJavascriptFile('home.js');
}


function fixJS($do_defered)
{
	global $context, $settings;

	unset($context['javascript_files']['jquery']);

	if (isset($context['javascript_files']['smf_editor']))
		return;

	foreach ($context['javascript_files'] as $id => &$js_file)
	{
		if ($id == 'smf_scripts')
			unset($context['javascript_files'][$id]);

		if (stristr($js_file['filename'], 'suggest.js') !== false)
			$js_file['filename'] = str_replace($settings['default_theme_url'], $settings['theme_url'], $js_file['filename']);
	}

	combineCSS($context['javascript_files']);
	loadJavascriptFile('script.js', array('default_theme' => true, 'defer' => false), 'smf_scripts');
}
function fixCSS()
{
	global $context, $settings;

	combineCSS($context['css_files']);
}

function combineCSS(&$files)
{
	global $context, $boarddir, $settings, $boardurl, $db_show_debug;

	//if ($db_show_debug === true)
	//	return;

	$css = $ext = '';
	$ids = array();
	$latest_date = 0;
	foreach ($files as $file)
	{
		if (strpos($file['filename'], '?action=') === false)
			$fname = strstr($file['filename'], '?', true);
		if (empty($fname))
			$fname = $file['filename'];
		$path = str_ireplace($boardurl, $boarddir, $fname);
		if (!file_exists($path))
			continue;
		$latest_date = max($latest_date, filemtime($path));
		$path_parts = pathinfo($path);
		$ids[$path] = !isset($file['options']['nolog']) ? $path_parts['filename'] : '';
		$ext = '.' . $path_parts['extension'];
	}
	$id = implode('-', array_filter($ids));
	$full_name = $id . '-' . $latest_date . $ext;
	$final_file = $settings['theme_dir'] . '/cache/' . $full_name;

	if (!file_exists($final_file))
	{
		require_once __DIR__ . '/src/Minify.php';
		require_once __DIR__ . '/src/Converter.php';
		require_once __DIR__ . '/src/JS.php';
		require_once __DIR__ . '/src/CSS.php';
		require_once __DIR__ . '/src/Exception.php';
		try {
			$files_to_delete = @glob($settings['theme_dir'] . '/cache/' . $id . '-*' . $ext);
			if (!empty($files_to_delete))
				foreach ($files_to_delete as $del)
					if (!strpos($del, $latest_date))
						@unlink($del);

			if ($ext == '.css')
				$minifier = new MatthiasMullie\Minify\CSS();
			else
				$minifier = new MatthiasMullie\Minify\JS();
			foreach ($ids as $path => $filename)
				$minifier->add($path);
			$minifier->minify($final_file);
		} catch (Exception $e) {
			log_error($e->getMessage());
		}
	}
	if ($ext == '.css')
		$files = array($id => array('filename' => $settings['theme_url'] . '/cache/' . $full_name, 'options' => array()));
	else
		$files = array(
			'smf_scripts' => array('filename' => $settings['theme_url'] . '/scripts/script.js', 'options' => array()),
			$id => array('filename' => $settings['theme_url'] . '/cache/' . $full_name, 'options' => ['async' => true])
		);
}

function members_ads_integrate_actions(&$action_array)
{
	$action_array['forum'] = array('BoardIndex.php', 'BoardIndex');
}

?>