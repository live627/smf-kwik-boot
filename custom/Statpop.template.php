<?php

function template_statpop()
{
	global $context, $txt;

	echo '
	<style>
		#stats_menu div {
			overflow-y: auto;
			max-height: 20em;
		}
	</style>
	<div>';

	foreach ($context['yearly'] as $year)
		foreach ($txt['months'] as $id => $month)
			echo '
		<label for="', $year, '-', $id, '_chk">
			<input type="checkbox" name="', $year, '-', $id, '_chk" id="', $year, '-', $id, '_chk" value="', $year, '-', $id, '"', !empty($context['search_params']['show_complete']) ? ' checked' : '', ' class="input_check" />', $month, ' ', $year, '
		</label><br>';

	echo '
	</div>
	<script>
		getUserInput();
	</script>';
}

?>