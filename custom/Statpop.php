<?php

function Statpop()
{
	global $context, $smcFunc, $txt, $db_show_debug;

	$db_show_debug = false;
	$context['template_layers'] = array();

	$request = $smcFunc['db_query']('', '
		SELECT
			YEAR(date) AS stats_year
		FROM {db_prefix}log_activity
		GROUP BY stats_year');

	$context['yearly'] = array();
	while ($row = $smcFunc['db_fetch_row']($request))
		$context['yearly'][] = $row[0];
	$smcFunc['db_free_result']($request);
}

?>