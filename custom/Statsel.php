<?php

function Statsel()
{
	global $context, $modSettings, $sourcedir;

	$context['sub_template'] = 'stats';
	$condition_text = array();
	$condition_params = array();
	$condition_text[] = 'YEAR(date) IN({array_int:years}) AND MONTH(date) IN ({array_int:months})';
	$condition_params['years'] = array_map('intval', array_keys($_GET['h']));
	$condition_params['months'] = array_map('intval', array_values($_GET['h']));

	// No daily stats to even look at?
	if (empty($condition_text))
		return;

	require_once($sourcedir . '/Stats.php');
	DisplayStats();
	getDailyStats(implode(' OR ', $condition_text), $condition_params);

	$y = array();
	$m = array();
	$d = array();
	$ms = false;
	foreach ($context['yearly'] as $id => $year)
		if (isset($condition_params['year_' . $id]))
		{
			$y['labels'][] = $id;
			$y['new_topics'][] = $year['new_topics'];
			$y['new_posts'][] = $year['new_posts'];
			$y['new_members'][] = $year['new_members'];
			$y['most_members_online'][] = $year['most_members_online'];

			if (!empty($modSettings['hitStats']))
				$y[$id]['hits'][] = $year['hits'];

			foreach ($year['months'] as $month)
				if (isset($condition_params['months_' . $id]) && in_array($month['date']['month'], $condition_params['months_' . $id]))
				{
					$m['labels'][] = $month['month'] . ' ' . $month['year'];
					$m['new_topics'][] = $month['new_topics'];
					$m['new_posts'][] = $month['new_posts'];
					$m['new_members'][] = $month['new_members'];
					$m['most_members_online'][] = $month['most_members_online'];

					if (!empty($modSettings['hitStats']))
						$m[$id]['hits'][] = $month['hits'];

					if (!$ms)
					{
						$ms = true;

						foreach ($month['days'] as $day)
						{
							$d['labels'][] = $day['day'];
							$d['new_topics'][] = $day['new_topics'];
							$d['new_posts'][] = $day['new_posts'];
							$d['new_members'][] = $day['new_members'];
							$d['most_members_online'][] = $day['most_members_online'];

							if (!empty($modSettings['hitStats']))
								$d['hits'][] = $day['hits'];
						}
					}
				}
		}

	echo '
			y = ' . json_encode($y) .  ',
			m = ' . json_encode($m) .  ',
			d = ' . json_encode($d) .  ';';
	die();
}

?>