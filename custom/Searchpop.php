<?php

function Searchpop()
{
	global $context, $modSettings, $txt, $db_show_debug;

	$db_show_debug = false;
	$context['template_layers'] = array();

	if (!empty($context['load_average']) && !empty($modSettings['loadavg_search']) && $context['load_average'] >= $modSettings['loadavg_search'])
		fatal_lang_error('loadavg_search_disabled', false);

	loadLanguage('Search');
}

?>