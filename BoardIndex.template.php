<?php
/**
 * Simple Machines Forum (SMF)
 *
 * @package SMF
 * @author Simple Machines http://www.simplemachines.org
 * @copyright 2014 Simple Machines and individual contributors
 * @license http://www.simplemachines.org/about/smf/license.php BSD
 *
 * @version 2.1 Alpha 1
 */

function template_boardindex_outer_above()
{
	global $context, $settings;

	// Show the news fader?  (assuming there are things to show...)
	if (!empty($settings['show_newsfader']) && !empty($context['news_lines']))
		template_newsfader();
}

function template_newsfader()
{
	global $context, $settings;

	echo '
		<ul id="smf_slider" class="panel panel-default panel-body">';

	foreach ($context['news_lines'] as $news)
	{
		echo '
			<li>', $news,'</li>';
	}

	echo '
		</ul>
		<script>
			jQuery("#smf_slider").slippry({
				speed: ', $settings['newsfader_time'],'
			});
		</script>';
}

function template_main()
{
	global $context, $txt, $scripturl;

	echo '
	<div id="boardindex_table" class="boardindex_table">';

	/* Each category in categories is made up of:
	id, href, link, name, is_collapsed (is it collapsed?), can_collapse (is it okay if it is?),
	new (is it new?), collapse_href (href to collapse/expand), collapse_image (up/down image),
	and boards. (see below.) */
	foreach ($context['categories'] as $category)
	{
		// If theres no parent boards we can see, avoid showing an empty category (unless its collapsed)
		if (empty($category['boards']) && !$category['is_collapsed'])
			continue;

		echo '
		<div class="panel panel-default">
			<div class="panel-heading" id="category_', $category['id'], '">
				<h3 class="panel-title">';

		// If this category even can collapse, show a link to collapse it.
		if ($category['can_collapse'])
			echo '
					<span id="category_', $category['id'], '_upshrink" class="', $category['is_collapsed'] ? 'toggle_down' : 'toggle_up', ' floatright" data-collapsed="', (int) $category['is_collapsed'], '" title="', !$category['is_collapsed'] ? $txt['hide_category'] : $txt['show_category'],'" align="bottom" style="display: none;"></span>';

		echo '
					', $category['link'], '
				</h3>', !empty($category['description']) ? '
				<div class="desc">' . $category['description'] . '</div>' : '', '
			</div>
			<div id="category_', $category['id'], '_boards" class="panel-body">';

		if ($category['id'] == 2)
		template_list_boards2($category['boards']);
		else
		template_list_boards($category['boards']);

		echo '
			</div>
		</div>';
	}

	echo '
	</div>';

	// Show the mark all as read button?
	if ($context['user']['is_logged'] && !empty($context['categories']))
		echo '
		<div class="mark_read">', template_button_strip($context['mark_read_button'], 'right'), '</div>';
}

function template_boardindex_outer_below()
{
	template_info_center();
}

function template_info_center()
{
	global $context, $options, $txt;

	if (empty($context['info_center']))
		return;

	// Here's where the "Info Center" starts...
	echo '<br>
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">', sprintf($txt['info_center_title'], $context['forum_name_html_safe']), '</h3>
						<span class="pull-right">
							<!-- Tabs -->
							<ul class="nav panel-tabs">
								<li class="active"><a href="#tab1" data-toggle="tab">', $txt['recent_posts'], '</a></li>
								<li><a href="#tab2" data-toggle="tab">', $context['calendar_only_today'] ? $txt['calendar_today'] : $txt['calendar_upcoming'], '</a></li>
								<li><a href="#tab3" data-toggle="tab">', $txt['forum_stats'], '</a></li>
								<li><a href="#tab4" data-toggle="tab">', $txt['online_users'], '</a></li>
							</ul>
						</span>
					</div>
					<div class="panel-body">
						<div class="tab-content">
							<div class="tab-pane row active" id="tab1">';

	template_ic_block_recent();

	echo '</div>
							<div class="tab-pane row" id="tab2">';

	template_ic_block_calendar();

	echo '</div>
							<div class="tab-pane row" id="tab3">';

	template_ic_block_stats();

	echo '</div>
							<div class="tab-pane row" id="tab4">';

	template_ic_block_online();

	echo ',</div>
						</div>
					</div>
				</div>';
}

function template_ic_block_recent()
{
	global $context, $scripturl, $settings, $txt;

	// This is the "Recent Posts" bar.
	echo '
			<div class="sub_bar">
				<h4 class="subbg">
					<a href="', $scripturl, '?action=recent"><span class="xx"></span>', $txt['recent_posts'], '</a>
				</h4>
			</div>
			<div id="recent_posts_content">';

	// Only show one post.
	if ($settings['number_recent_posts'] == 1)
	{
		// latest_post has link, href, time, subject, short_subject (shortened with...), and topic. (its id.)
		echo '
				<p id="infocenter_onepost" class="inline">
					<a href="', $scripturl, '?action=recent">', $txt['recent_view'], '</a>&nbsp;&quot;', sprintf($txt['is_recent_updated'], '&quot;' . $context['latest_post']['link'], '&quot;'), ' (', $context['latest_post']['time'], ')<br>
				</p>';
	}
	// Show lots of posts.
	elseif (!empty($context['latest_posts']))
	{
		echo '
				<table id="ic_recentposts" class="table table-striped">
					<tr>
						<th class="recentpost first_th">', $txt['message'], '</th>
						<th class="recentposter">', $txt['author'], '</th>
						<th class="recentboard">', $txt['board'], '</th>
						<th class="recenttime last_th">', $txt['date'], '</th>
					</tr>';

		/* Each post in latest_posts has:
				board (with an id, name, and link.), topic (the topic's id.), poster (with id, name, and link.),
				subject, short_subject (shortened with...), time, link, and href. */
		foreach ($context['latest_posts'] as $post)
			echo '
					<tr>
						<td class="recentpost"><strong>', $post['link'], '</strong></td>
						<td class="recentposter">', $post['poster']['link'], '</td>
						<td class="recentboard">', $post['board']['link'], '</td>
						<td class="recenttime">', $post['time'], '</td>
					</tr>';
		echo '
				</table>';
	}
	echo '
			</div>';
}

function template_ic_block_calendar()
{
	global $context, $scripturl, $txt, $settings;

	// Show information about events, birthdays, and holidays on the calendar.
	echo '
			<div class="sub_bar">
				<h4 class="subbg">
					<a href="', $scripturl, '?action=calendar' . '"><span class="generic_icons calendar"></span> ', $context['calendar_only_today'] ? $txt['calendar_today'] : $txt['calendar_upcoming'], '</a>
				</h4>
			</div>';

	// Holidays like "Christmas", "Chanukah", and "We Love [Unknown] Day" :P.
	if (!empty($context['calendar_holidays']))
		echo '
			<div class="list-group col-xs-6 col-sm-4">
				<div class="list-group-item">
					<h4 class="list-group-item-heading">', $txt['calendar_prompt'], '</h4>
				</div>
				<p class="list-group-item">', implode('</p>
				<p class="list-group-item">', $context['calendar_holidays']), '</p>
			</div>';

	// People's birthdays. Like mine. And yours, I guess. Kidding.
	if (!empty($context['calendar_birthdays']))
	{
		echo '
			<div class="list-group col-xs-6 col-sm-4">
				<div class="list-group-item">
					<h4 class="list-group-item-heading">
						', $context['calendar_only_today'] ? $txt['birthdays'] : $txt['birthdays_upcoming'], '
					</h4>
				</div>';
		// Each member in calendar_birthdays has: id, name (person), age (if they have one set?), is_last. (last in list?), and is_today (birthday is today?)
		foreach ($context['calendar_birthdays'] as $member)
			echo '
				<a class="list-group-item" href="', $scripturl, '?action=profile;u=', $member['id'], '">', $member['is_today'] ? '<strong class="fix_rtl_names">' : '', $member['name'], $member['is_today'] ? '</strong>' : '', isset($member['age']) ? ' (' . $member['age'] . ')' : '', '</a>';
		echo '
			</div>';
	}

	// Events like community get-togethers.
	if (!empty($context['calendar_events']))
	{
		echo '
			<div class="list-group col-xs-6 col-sm-4">
				<div class="list-group-item">
					<h4 class="list-group-item-heading">
						', $context['calendar_only_today'] ? $txt['events'] : $txt['events_upcoming'], '
					</h4>
				</div>';

		// Each event in calendar_events should have:
		//		title, href, is_last, can_edit (are they allowed?), modify_href, and is_today.
		foreach ($context['calendar_events'] as $event)
			echo '
					', $event['href'] == '' ? '<p class="list-group-item">' : '<a class="list-group-item" href="' . $event['href'] . '">', $event['is_today'] ? '<strong>' . $event['title'] . '</strong>' : $event['title'], $event['href'] == '' ? '</p>' : '</a>';
		echo '
				</p>';
	}

	echo '
			</div>';
}

function template_ic_block_stats()
{
	global $scripturl, $txt, $context, $settings;

	// Show statistical style information...
	echo '
			<div class="sub_bar">
				<h4 class="subbg">
					<a href="', $scripturl, '?action=stats" title="', $txt['more_stats'], '"><span class="generic_icons stats"></span> ', $txt['more_stats'], '</a>
				</h4>
			</div>
			<div class="row">
				<div class="hero-widget col-sm-3">
					<var>', $context['common_stats']['total_posts'], '</var>
					<label class="text-muted">', $txt['posts'], '</label>
				</div>
				<div class="hero-widget col-sm-3">
					<var>', $context['common_stats']['total_topics'], '</var>
					<label class="text-muted">', $txt['topics'], '</label>
				</div>
				<div class="hero-widget col-sm-3">
					<var>', $context['common_stats']['total_members'], '</var>
					<label class="text-muted">', $txt['members'], '</label>
				</div>
				<div class="hero-widget col-sm-3">
					<var>', $context['common_stats']['total_posts'], '</var>
					<label class="text-muted">', $txt['posts'], '</label>
				</div>
			</div>

			<div class="row">
				<div class="list-group col-xs-6">
					<a class="list-group-item" href="' . $context['latest_post']['href'] . '">
						<h4 class="list-group-item-heading">', $txt['latest_post'], '</h4>
						<p class="list-group-item-text">' . $context['latest_post']['subject'] . '<br><label class="text-muted">' . $context['latest_post']['time'] . '</label></p>
					</a>
					<a class="list-group-item" href="', $scripturl, '?action=recent">', $txt['recent_view'], '</a>
				</div>
				<div class="list-group col-xs-6">
					<a class="list-group-item" href="' . $context['common_stats']['latest_member']['href'] . '">
						<h4 class="list-group-item-heading">', $txt['latest_member'], '</h4>
						<p class="list-group-item-text">' . $context['common_stats']['latest_member']['name'] . '<br><label class="text-muted">' . $context['latest_post']['time'] . '</label></p>
					</a>
				</div>
			</div>';
}

function template_ic_block_online()
{
	global $context, $scripturl, $txt, $modSettings, $settings;
	// "Users online" - in order of activity.
	echo '
			<div class="sub_bar">';

	if ($context['show_who'])
		echo '
				<h4 class="subbg">
					<a href="', $scripturl, '?action=who" title="', $txt['more_stats'], '"><span class="generic_icons stats"></span> ', $txt['more_stats'], '</a>
				</h4>';

	echo '
			</div>
			<div class="row">
				<div class="hero-widget col-sm-2">
					<var>', comma_format($context['num_users_online']), '</var>
					<label class="text-muted">', $context['num_users_online'] == 1 ? $txt['user'] : $txt['users'], '</label>
				</div>';

	if ($context['show_buddies'])
		echo '
				<div class="hero-widget col-sm-2">
					<var>', comma_format($context['num_buddies']), '</var>
					<label class="text-muted">', $context['num_buddies'] == 1 ? $txt['buddy'] : $txt['buddies'], '</label>
				</div>';

	echo '
				<div class="hero-widget col-sm-2">
					<var>', comma_format($context['num_guests']), '</var>
					<label class="text-muted">', $context['num_guests'] == 1 ? $txt['guest'] : $txt['guests'], '</label>
				</div>';

	if (!empty($context['num_users_hidden']))
		echo '
				<div class="hero-widget col-sm-2">
					<var>', comma_format($context['num_spiders']), '</var>
					<label class="text-muted">', $context['num_spiders'] == 1 ? $txt['spider'] : $txt['spiders'], '</label>
				</div>';

	if (!empty($context['num_users_hidden']))
		echo '
				<div class="hero-widget col-sm-2">
					<var>', comma_format($context['num_users_hidden']), '</var>
					<label class="text-muted">', $context['num_users_hidden'] == 1 ? $txt['hidden'] : $txt['hidden_s'], '</label>
				</div>
			</div>';

	if (!empty($context['users_online']) && !empty($context['online_groups']))
	{
		echo '
			<div class="row">';

		if (!empty($context['users_online']))
		{
			echo '
				<div class="list-group col-xs-9 row">';

			foreach ($context['users_online'] as $user)
				echo '
					<a class="list-group-item col-md-4 col-xs-6" href="' . $user['href'] . '">
						<h4 class="list-group-item-heading">', $user['name'], '</h4>
						<p class="list-group-item-text">' . $context['online_groups'][$user['group']]['name'] . '</p>
					</a>';
		}
		echo '
				</div>';

		if (!empty($context['online_groups']))
		{
			echo '
				<div class="list-group col-xs-3">';

			foreach ($context['online_groups'] as $group)
				echo '
					<a class="list-group-item col-md-4 col-xs-6" href="' . $scripturl . '?action=groups;sa=members;group=' . $group['id'] . '">', $group['name'], '</a>';
		}
		echo '
				</div>';

		echo '
			</div>
			(', sprintf($txt['users_active'], $modSettings['lastActive']), ')';
	}
}
?>