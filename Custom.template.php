<?php

function Searchpop()
{
	global $context, $modSettings, $txt, $db_show_debug;

	$db_show_debug = false;
	$context['template_layers'] = array();

	if (!empty($context['load_average']) && !empty($modSettings['loadavg_search']) && $context['load_average'] >= $modSettings['loadavg_search'])
		fatal_lang_error('loadavg_search_disabled', false);

	loadLanguage('Search');
}

function template_searchpop()
{
	global $context, $settings, $txt, $scripturl;

		echo '
				<dl class="settings" id="search_options">
					<dt class="righttext"><label for="searchtype">',
						$txt['search_match'], ':</label>
					</dt>
					<dd>
						<select name="searchtype" id="searchtype">
							<option value="1"', empty($context['search_params']['searchtype']) ? ' selected="selected"' : '', '>', $txt['all_words'], '</option>
							<option value="2"', !empty($context['search_params']['searchtype']) ? ' selected="selected"' : '', '>', $txt['any_words'], '</option>
						</select>
					</dd>
					<dt class="righttext"><label for="userspec">',
						$txt['by_user'], ':</label>
					</dt>
					<dd>
						<input id="userspec" type="text" name="userspec" value="', empty($context['search_params']['userspec']) ? '*' : $context['search_params']['userspec'], '" size="30" class="input_text" />
					</dd>
					<dt class="righttext"><label for="sort">',
						$txt['search_order'], ':</label>
					</dt>
					<dd>
						<select id="sort" name="sort">
							<option value="relevance|desc">', $txt['search_orderby_relevant_first'], '</option>
							<option value="num_replies|desc">', $txt['search_orderby_large_first'], '</option>
							<option value="num_replies|asc">', $txt['search_orderby_small_first'], '</option>
							<option value="id_msg|desc">', $txt['search_orderby_recent_first'], '</option>
							<option value="id_msg|asc">', $txt['search_orderby_old_first'], '</option>
						</select>
					</dd>
					<dt class="righttext options">',
						$txt['search_options'], ':
					</dt>
					<dd class="options">
						<label for="show_complete">
							<input type="checkbox" name="show_complete" id="show_complete" value="1"', !empty($context['search_params']['show_complete']) ? ' checked' : '', ' class="input_check" />', $txt['search_show_complete_messages'], '
						</label><br />
						<label for="subject_only">
							<input type="checkbox" name="subject_only" id="subject_only" value="1"', !empty($context['search_params']['subject_only']) ? ' checked' : '', ' class="input_check" />', $txt['search_subject_only'], '
						</label>
					</dd>
					<dt class="righttext between">',
						$txt['search_post_age'], ':
					</dt>
					<dd><label for="minage">',
						$txt['search_between'], '</label> <input type="text" name="minage" id="minage" value="', empty($context['search_params']['minage']) ? '0' : $context['search_params']['minage'], '" size="5" maxlength="4" class="input_text" />&nbsp;<label for="maxage">', $txt['search_and'], '&nbsp;</label><input type="text" name="maxage" id="maxage" value="', empty($context['search_params']['maxage']) ? '9999' : $context['search_params']['maxage'], '" size="5" maxlength="4" class="input_text" /> ', $txt['days_word'], '
					</dd>
				</dl>
				<input type="hidden" name="advanced" value="1" />';

		// Require an image to be typed to save spamming?
		if ($context['require_verification'])
		{
			echo '
				<p>
					<strong>', $txt['verification'], ':</strong>
					', template_control_verification($context['visual_verification_id'], 'all'), '
				</p>';
		}

		// If $context['search_params']['topic'] is set, that means we're searching just one topic.
		if (!empty($context['search_params']['topic']))
			echo '
				<p>', $txt['search_specific_topic'], ' &quot;', $context['search_topic']['link'], '&quot;.</p>
				<input type="hidden" name="topic" value="', $context['search_topic']['id'], '" />';

		echo '
			</div>
		</fieldset>
		';

}

function Statsel()
{
	global $context, $modSettings, $sourcedir;

	$context['sub_template'] = 'stats';
	$condition_text = array();
	$condition_params = array();
	$condition_text[] = 'YEAR(date) IN({array_int:years}) AND MONTH(date) IN ({array_int:months})';
	$condition_params['years'] = array_map('intval', array_keys($_GET['h']));
	$condition_params['months'] = array_map('intval', array_values($_GET['h']));

	// No daily stats to even look at?
	if (empty($condition_text))
		return;

	require_once($sourcedir . '/Stats.php');
	DisplayStats();
	getDailyStats(implode(' OR ', $condition_text), $condition_params);

	$y = array();
	$m = array();
	$d = array();
	$ms = false;
	foreach ($context['yearly'] as $id => $year)
		if (isset($condition_params['year_' . $id]))
		{
			$y['labels'][] = $id;
			$y['new_topics'][] = $year['new_topics'];
			$y['new_posts'][] = $year['new_posts'];
			$y['new_members'][] = $year['new_members'];
			$y['most_members_online'][] = $year['most_members_online'];

			if (!empty($modSettings['hitStats']))
				$y[$id]['hits'][] = $year['hits'];

			foreach ($year['months'] as $month)
				if (isset($condition_params['months_' . $id]) && in_array($month['date']['month'], $condition_params['months_' . $id]))
				{
					$m['labels'][] = $month['month'] . ' ' . $month['year'];
					$m['new_topics'][] = $month['new_topics'];
					$m['new_posts'][] = $month['new_posts'];
					$m['new_members'][] = $month['new_members'];
					$m['most_members_online'][] = $month['most_members_online'];

					if (!empty($modSettings['hitStats']))
						$m[$id]['hits'][] = $month['hits'];

					if (!$ms)
					{
						$ms = true;

						foreach ($month['days'] as $day)
						{
							$d['labels'][] = $day['day'];
							$d['new_topics'][] = $day['new_topics'];
							$d['new_posts'][] = $day['new_posts'];
							$d['new_members'][] = $day['new_members'];
							$d['most_members_online'][] = $day['most_members_online'];

							if (!empty($modSettings['hitStats']))
								$d['hits'][] = $day['hits'];
						}
					}
				}
		}

	echo '
			y = ' . json_encode($y) .  ',
			m = ' . json_encode($m) .  ',
			d = ' . json_encode($d) .  ';';
	die();
}

function Statpop()
{
	global $context, $smcFunc, $txt, $db_show_debug;

	$db_show_debug = false;
	$context['template_layers'] = array();

	$request = $smcFunc['db_query']('', '
		SELECT
			YEAR(date) AS stats_year
		FROM {db_prefix}log_activity
		GROUP BY stats_year');

	$context['yearly'] = array();
	while ($row = $smcFunc['db_fetch_row']($request))
		$context['yearly'][] = $row[0];
	$smcFunc['db_free_result']($request);
}

function template_statpop()
{
	global $context, $txt;

	echo '
	<style>
		#stats_menu div {
			overflow-y: auto;
			max-height: 20em;
		}
	</style>
	<div>';

	foreach ($context['yearly'] as $year)
		foreach ($txt['months'] as $id => $month)
			echo '
		<label for="', $year, '-', $id, '_chk">
			<input type="checkbox" name="', $year, '-', $id, '_chk" id="', $year, '-', $id, '_chk" value="', $year, '-', $id, '"', !empty($context['search_params']['show_complete']) ? ' checked' : '', ' class="input_check" />', $month, ' ', $year, '
		</label><br>';

	echo '
	</div>
	<script>
		getUserInput();
	</script>';
}

?>