<?php
// Version: 2.0 RC3; ThemeStrings

// Important! Before editing these language files please read the text at the top of index.english.php.
$txt['forum'] = 'Forum';
$txt['buddy'] = 'Friend';
$txt['buddies'] = 'Friends';
$txt['find_buddies'] = 'Show Friends Only?';
$txt['approve_members_waiting'] = 'Awaiting approval';

$txt['edited'] = 'edited';

$txt['showfriendbutton'] = 'Show the Friend menu button?';
$txt['display'] = 'Layout for Display template:';
$txt['blogboards'] = 'Boards to show blog template';
$txt['blogboards2'] = 'Add the Board id\'s to show blog template only on those boards, separated by comma(1,4,7). Leave <b>empty</b> to show blog template on ALL boards.';
$txt['avatarboards'] = 'Do not show avatars on Boardindex and MessageIndex';
$txt['area'] = 'Insert your own HTML code';
$txt['area1'] = 'Where to show HTML-box';
$txt['none'] = '-none-';
$txt['copy'] = 'Under the copyright';
$txt['after'] = 'After first post';
$txt['between'] = 'Between each post';
$txt['sidebar'] = 'Boardindex Sidebar';
$txt['google'] = 'Google tracking code';
$txt['facebook'] = 'Show Facebook like button in topics';
$txt['twitter'] = 'Show Tweet button in topics (needs a Twitter account specified)';
$txt['tooltips'] = 'Do not show tooltips when hovering over a topic\'s title';
$txt['twitterbox'] = 'Show from Twitter';
$txt['twitterbox2'] = 'Add the twitter ACCOUNT you wish to show latest tweets from. Shown inside InfoCenter.';
$txt['twittercount'] = 'Number of tweets to show(set 0 to not show)';
$txt['pm'] = 'PM';
$txt['replies'] = 'Replies';
$txt['unread'] = 'Unread';

$txt['unapproved'] = 'Unapproved';
$txt['reports'] = 'Reports';
$txt['followus'] = 'Follow us on Twitter';
$txt['fromtwitter'] = 'From Twitter';
$txt['custom'] = 'Custom theme options';

$txt['contact'] = 'Contact';
$txt['contact_from_blank'] = 'You need to enter your name.';
$txt['contact_email_blank'] = 'You need to enter your email address.';
$txt['contact_subject_blank'] = 'You need to enter a subject.';
$txt['contact_message_blank'] = 'You need to enter a message.';
$txt['contact_title'] = 'Contact Us';
$txt['contact_title_sent'] = 'Message Sent';
$txt['contact_field_from'] = 'Name';
$txt['contact_field_email'] = 'Email';
$txt['contact_field_subject'] = 'Subject';
$txt['contact_field_message'] = 'Message';
$txt['contact_field_submit'] = 'Send Email';
$txt['contact_success'] = 'Your message has been sent!';
$txt['contact_success_link'] = 'Return to the message board';
$txt['whoall_contact'] = 'Filling out the <a href="' . $scripturl . '?action=contact">contact</a> form.';

?>